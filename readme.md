# Mobius Base Cheat

Ever since I've started working on my new base I've wanted to release a basehook for people to work with. Gir named the project Mobius (because it sounds cool) and finally gave me the push to start working on it.

## Downloads

It is currently not ready to be actively used as a base.

[BitBucket repository](https://bitbucket.org/CursedGhost/mobiusbase)

## Building

Requires Visual Studio 2010 or higher.
Download and compile the dependencies first!

## Dependencies

Requires my [LibEx library](https://bitbucket.org/CursedGhost/libex) for a variety of tools. After getting it make sure to run install.bat which creates a user environment variable LIBEX_PATH which is required so Mobius can find it!

Compile both Debug and Release versions of LibEx and you're done. Do let me know about any compiler errors!
