#pragma once

//----------------------------------------------------------------
// Basic cheat interface
//----------------------------------------------------------------

#include "shared.h"

namespace mobius
{

class CCheatManager;
class CMobiusBase;
class CBaseVars;
class ICheat;


// Callback functions must be registered individually in the cheat manager.
// Provide the cheat index, a priority, callback type and the function to be called.

// Callback types
enum cbt_t
{
	CBT_TICK = 1,	// Proper tick callback with predicted everything
	CBT_UPDATE,		// Every frame
	CBT_PAINT,		// Drawing stuff
	CBT_EVENT,		// Event callback
	CBT_VIEW,		// Modify the view setup before rendering
	CBT_CONNECT,	// Connect events
	CBT_MESSAGE,	// User messages
	CBT_SOUND,		// Sound emitted
	CBT_COUNT,
};

// Provides a base interface for cheat modules
class ICheat
{
	friend class CCheatManager;

public:
	virtual ~ICheat() {}

	// Initialization context
	struct init_t {
		CCheatManager* mgr;
		CMobiusBase* base;
	};

	// Initialization: Register your cvars, callbacks here and read resources
	virtual void Init(init_t& vars) = 0;

	// Return an identifier for this cheat
	virtual const char* Name() const = 0;

private:
	ICheat* next = nullptr;
};

// Callback function pointer signature
typedef void (*CallbackFn)(ICheat*, CBaseVars&);

// Convenience wrapper to dispatch to member function
template<typename Type, typename Vars, void(Type::*Delegate)(Vars&)>
inline void CallbackThunk(ICheat* cheat, CBaseVars& vars) {
	(static_cast<Type*>(cheat)->*Delegate)(static_cast<Vars&>(vars));
}

struct callback_t
{
	callback_t(ICheat* inst, int priority, cbt_t ttype, CallbackFn callback)
		: inst(inst), priority(priority), type(type), callback(callback), next(nullptr) {}

	ICheat* inst;
	int priority;
	cbt_t type;
	CallbackFn callback;
	callback_t* next;
};

}
