#pragma once

typedef void* (__cdecl* CreateInterfaceFn)( const char*, int* );

class bf_read;
class bf_write;

typedef void (__cdecl*UserMsgHookFn)( bf_read &msg );

#include "sdk/const.h"
#include "sdk/globalvars.h"
#include "sdk/vector.h"
#include "sdk/mathlib.h"
#include "sdk/inputsystem.h"
#include "sdk/viewsetup.h"
#include "sdk/gameevents.h"
#include "sdk/gamemovement.h"
#include "sdk/cdll_int.h"
#include "sdk/recvstuff.h"
#include "sdk/trace.h"
#include "sdk/cliententitylist.h"
#include "sdk/cliententity.h"
#include "sdk/convar.h"
#include "sdk/enginetrace.h"
#include "sdk/tf_weapon_parse.h"
#include "sdk/isurface.h"
#include "sdk/debugoverlay.h"
#include "sdk/icvar.h"
#include "sdk/modelinfo.h"
#include "sdk/netchannelinfo.h"
#include "sdk/netchannel.h"
#include "sdk/studio.h"

#include "sdk/steam/isteamfriends.h"

// From tier0.dll
extern "C" __declspec(dllimport) void	RandomSeed( int iSeed );
extern "C" __declspec(dllimport) float	RandomFloat( float flMinVal = 0.0f, float flMaxVal = 1.0f );
extern "C" __declspec(dllimport) float	RandomFloatExp( float flMinVal = 0.0f, float flMaxVal = 1.0f, float flExponent = 1.0f );
extern "C" __declspec(dllimport) int	RandomInt( int iMinVal, int iMaxVal );
extern "C" __declspec(dllimport) float	RandomGaussianFloat( float flMean = 0.0f, float flStdDev = 1.0f );
extern "C" __declspec(dllimport) void	ConColorMsg( int, const unsigned int&, const char*, ... );

class IConVar;
class FileWeaponInfo_t;

struct Ray_t
{
	VectorAligned  m_Start;	// starting point, centered within the extents
	VectorAligned  m_Delta;	// direction + length of the ray
	VectorAligned  m_StartOffset;	// Add this to m_Start to get the actual ray start
	VectorAligned  m_Extents;	// Describes an axis aligned box extruded along a ray
	bool	m_IsRay;	// are the extents zero?
	bool	m_IsSwept;	// is delta != 0?

};

struct CUserMessage
{
	int size;
	const char* name;
	// CUtlMemory
	UserMsgHookFn* m_pMemory;
	int m_nAllocationCount;
	int m_nGrowSize;
	// CUtlVector
	int m_nSize;
	UserMsgHookFn* m_pElements;
};

struct old_bf_read
{
	const unsigned char*	m_pData;
	int						m_nDataBytes;
	int						m_nDataBits;
	int				m_iCurBit;
	bool			m_bOverflow;
	bool			m_bAssertOnOverflow;
	const char		*m_pDebugName;
};
class bf_read : public old_bf_read
{
};

class INetMessageHandler;
class IServerMessageHandler;
class IClientMessageHandler;
class CNetMessage
{
public:
	void** vtable;
	bool bReliable;
	INetChannel* pNetChannel;
	INetMessageHandler* pHandler;
};
class SVC_Print : public CNetMessage
{
public:
	const char* text;
};
class NET_StringCmd : public CNetMessage
{
public:
	const char* command;
	unsigned int m5;
	unsigned int m6;
	unsigned int m7;
};
class SVC_GetCvarValue : public CNetMessage
{
public:
	const char* cvar;
	unsigned int m6;
	unsigned int m7;
	unsigned int m8;
	unsigned int m9;
};

typedef struct player_info_s
{
	// scoreboard information
	char			name[MAX_PLAYER_NAME_LENGTH];
	// local server user ID, unique while server is running
	int				userID;
	// global unique player identifer
	char			guid[SIGNED_GUID_LEN + 1];
	// friends identification number
	unsigned int	friendsID;
	// friends name
	char			friendsName[MAX_PLAYER_NAME_LENGTH];
	// true, if player is a bot controlled by game.dll
	bool			fakeplayer;
	// true if player is the HLTV proxy
	bool			ishltv;
} player_info_t;

class IRecipientFilter;

typedef struct con_nprint_s
{
	int		index;			// Row #
	float	time_to_live;	// # of seconds before it disappears. -1 means to display for 1 frame then go away.
	float	color[ 3 ];		// RGB colors ( 0.0 -> 1.0 scale )
	bool	fixed_width_font;
} con_nprint_t;

enum CommandButtons
{
	IN_ATTACK = (1 << 0),
	IN_JUMP = (1 << 1),
	IN_DUCK = (1 << 2),
	IN_FORWARD = (1 << 3),
	IN_BACK = (1 << 4),
	IN_USE = (1 << 5),
	IN_CANCEL = (1 << 6),
	IN_LEFT = (1 << 7),
	IN_RIGHT = (1 << 8),
	IN_MOVELEFT = (1 << 9),
	IN_MOVERIGHT = (1 << 10),
	IN_ATTACK2 = (1 << 11),
	IN_RUN = (1 << 12),
	IN_RELOAD = (1 << 13),
	IN_ALT1 = (1 << 14),
	IN_ALT2 = (1 << 15),
	IN_SCORE = (1 << 16),	// Used by client.dll for when scoreboard is held down
	IN_SPEED = (1 << 17),	// Player is holding the speed key
	IN_WALK = (1 << 18),	// Player holding walk key
	IN_ZOOM = (1 << 19),	// Zoom key for HUD zoom
	IN_WEAPON1 = (1 << 20),	// weapon defines these bits
	IN_WEAPON2 = (1 << 21),	// weapon defines these bits
	IN_BULLRUSH = (1 << 22),
};

enum SignonState
{
	SIGNONSTATE_NONE = 0,
	SIGNONSTATE_CHALLENGE = 1,
	SIGNONSTATE_CONNECTED = 2,
	SIGNONSTATE_NEW = 3,
	SIGNONSTATE_PRESPAWN = 4,
	SIGNONSTATE_SPAWN = 5,
	SIGNONSTATE_FULL = 6,
	SIGNONSTATE_CHANGELEVEL = 7
};

class CViewSetup;

class CUserCmd
{
public:
	virtual ~CUserCmd() {};

	// For matching server and client commands for debugging
	int		command_number;
	// the tick the client created this command
	int		tick_count;
	// Player instantaneous view angles.
	QAngle	viewangles;
	// Intended velocities
	//	forward velocity.
	float	forwardmove;   
	//  sideways velocity.
	float	sidemove;      
	//  upward velocity.
	float	upmove;         
	// Attack button states
	int		buttons;		
	// Impulse command issued.
	unsigned char    impulse;        
	// Current weapon id
	int		weaponselect;	
	int		weaponsubtype;

	int		random_seed;	// For shared random functions

	short	mousedx;		// mouse accum in x from create move
	short	mousedy;		// mouse accum in y from create move

	// Client only, tracks whether we've predicted this command at least once
	bool	hasbeenpredicted;
};

class Color;

class IGameResources
{
public:
	virtual	~IGameResources() {};

	// Team data access 
	virtual const char		*GetTeamName( int index ) = 0;
	virtual int				GetTeamScore( int index ) = 0;
	virtual const Color&	GetTeamColor( int index ) = 0;

	// Player data access
	virtual bool	IsConnected( int index ) = 0;
	virtual bool	IsAlive( int index ) = 0;
	virtual bool	IsFakePlayer( int index ) = 0;
	virtual bool	IsLocalPlayer( int index ) = 0;

	virtual const char *GetPlayerName( int index ) = 0;
	virtual int		GetPlayerScore( int index ) = 0;
	virtual int		GetPing( int index ) = 0;
//	virtual int		GetPacketloss( int index ) = 0;
	virtual int		GetDeaths( int index ) = 0;
	virtual int		GetFrags( int index ) = 0;
	virtual int		GetTeam( int index ) = 0;
	virtual int		GetHealth( int index ) = 0;
};

// Implement this class and register with entlist to receive entity create/delete notification
class C_BaseEntity;
class IClientEntityListener
{
public:
	virtual void OnEntityCreated( C_BaseEntity *pEntity ) {};
	//virtual void OnEntitySpawned( C_BaseEntity *pEntity ) {};
	virtual void OnEntityDeleted( C_BaseEntity *pEntity ) {};
};
