#include "stdafx.h"
#include "hooks.h"

namespace mobius
{

volatile long CHookManager::g_hooker = 0;

CHookManager::CHookManager() {}
CHookManager::~CHookManager()
{
	// If this is non-zero a crash is imminent...
	assert(g_hooker == 0);
}
void CHookManager::HookBegin()
{
	_InterlockedIncrement(&g_hooker);
}
void CHookManager::HookEnd()
{
	_InterlockedDecrement(&g_hooker);
}
bool CHookManager::WaitForHooks(unsigned int wait)
{
	for (unsigned int i = 0; i < wait; ++i) {
		::Sleep(1);
		if (!g_hooker) {
			return true;
		}
	}
	// Still a hook running OR you forgot to call HookEnd() somewhere!
	assert(false);
	return false;
}

}
