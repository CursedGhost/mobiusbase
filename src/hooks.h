#pragma once

#include "mobius.h"
#include "tools/vmthooks.h"

namespace mobius
{

class CHookManager
{
public:
	typedef CVMTManager hook_t;

	CHookManager();
	virtual ~CHookManager();

	virtual void Rehook() = 0;
	virtual void Unhook() = 0;

	// First thing to call in your hook
	static void HookBegin();
	// Last thing to call in your hook
	static void HookEnd();
	// Will wait until no hooks are currently executing.
	static bool WaitForHooks(unsigned int wait = 100);

private:
	// Keep track of how many hooks are currently executing
	static volatile long g_hooker;
};

}
