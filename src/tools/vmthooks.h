#pragma once

//----------------------------------------------------------------
// Toolkit: Virtual method table hooks
//----------------------------------------------------------------
// This code is public domain but credits (to Casual_Hacker on www.gamedeception.net) are always appreciated.

// nonstandard extension used: zero-sized array in struct/union
#pragma warning(disable: 4200)

namespace mobius
{

//----------------------------------------------------------------
// Class: VMTManager
//----------------------------------------------------------------
// The VMTManager class hooks by replacing the vmt pointer with a custom crafted one.
// Only calls through this instance will be hooked.
//
// A general hint:
// Abuse the __fastcall convention ( look it up on the msdn ) to catch the this pointer.
class CVMTManager
{
	// Forbid copy constructing and assignment.
	CVMTManager(const CVMTManager&);
	CVMTManager& operator= (const CVMTManager&);

public:
	/// C/C++ does not have void function pointers, while technically illegal, this will have to do.
	typedef void* pfn_t;

	typedef pfn_t vftable_t[];

	struct hktable_t
	{
		CVMTManager* self; // Link back to the hook instance
		uintptr_t guard; // Allows us to identify a hooked vftable, as it's unlikely that the guard is there by coincidence
		void* RTTIptr; // May or may not contain a valid RTTI ptr, better safe than sorry
		vftable_t vftable; // Vftable starts here
	};

	/// Guard identifies if an instance is hooked.
	enum { GUARD = 0xD34DB33F };

	/// Count the number of functions in a vftable.
	/// \param pVMT Pointer to the vftable.
	/// \return Number of virtual functions in the vftable.
	static size_t CountFuncs(vftable_t* vftable);

	/// Constructor. Does NOT hook the vftable, you must call Rehook() before the hooks become active!
	/// \param inst Pointer to a class instance to hook.
	/// \param offset Offset of the vftable in the instance.
	/// \param vfuncs Number of virtual functions in the vftable, a value of zero will make it count them itself.
	CVMTManager(void* inst, size_t offset = 0, size_t vfuncs = 0);

	/// Destructor.
	~CVMTManager();

	/// Hooks a function by index.
	/// \param newfunc Pointer to the new function.
	/// \param index Index of the virtual function to hook.
	CVMTManager& HookMethod(pfn_t newfunc, size_t index);

	/// Unhooks a function by index.
	/// \param index Index of the virtual function to unhook.
	CVMTManager& UnhookMethod(size_t index);

	/// Erases all hooks.
	void EraseHooks();

	/// Return the original vftable. Instance will appear completely unhooked,
	/// however the hooks themselves are kept and can be activated again with Rehook().
	CVMTManager& Unhook();

	/// Replace the vftable with hooks.
	CVMTManager& Rehook();

	/// Tests if the instance is currently hooked.
	bool Hooked() const;

	/// Get the original virtual function pointer for an index.
	/// Use a function prototype for the template argument to make it very easy to call this function.
	/// Example syntax: hook.GetMethod<bool (__thiscall*)(Class*,int)>(12)(inst, 42);
	/// \tparam Fn Function prototype of the original virtual function.
	/// \param index Index of the virtual function to retrieve.
	/// \return Pointer to the original function.
	template<typename Fn>
	const Fn& GetMethod(size_t index) const;

	/// Get the userdata associated with this hook.
	template<typename T>
	T* GetUserData() const;

	/// Set the userdata associated with this hook.
	void SetUserData(void* data);

	/// Test if the instance is currently hooked.
	/// \param inst Instance to test if it is hooked.
	/// \param offset Offset of the vftable in the instance.
	/// \return True of the vftable is hooked.
	static bool HookPresent(void* inst, size_t offset = 0);

	/// Get the hook object associated with this instance.
	/// Does not actually check if the instance is hooked, check with HookPresent()!
	/// \param inst Instance to retrieve the hook object from.
	/// \param offset Offset of the vftable in the instance.
	/// \return Reference to the associated hook object.
	static CVMTManager& GetHook(void* inst, size_t offset = 0);

protected:
	static vftable_t*& _getvtbl(void* inst, size_t offset);

protected:
	vftable_t** _vftable;
	vftable_t* _oldvmt;
	hktable_t* _table;
	void* _userdata;
};

template<typename Fn>
inline const Fn& CVMTManager::GetMethod(size_t index) const
{
	assert(index < CountFuncs(_array + 3));
	return *(const Fn*)&_oldvmt[index];
}
inline CVMTManager& CVMTManager::Unhook()
{
	*_vftable = _oldvmt;
	return *this;
}
inline CVMTManager& CVMTManager::Rehook()
{
	*_vftable = &_table->vftable;
	return *this;
}
inline bool CVMTManager::Hooked() const
{
	return *_vftable != _oldvmt;
}
template<typename T>
inline T* CVMTManager::GetUserData() const
{
	return reinterpret_cast<T*>(_userdata);
}
inline void CVMTManager::SetUserData(void* data)
{
	_userdata = data;
}

}
