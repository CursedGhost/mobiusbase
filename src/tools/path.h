#pragma once

//----------------------------------------------------------------
// Filename Paths
//----------------------------------------------------------------
// Somewhat based on boost::filesystem::Path but uses plain C array as buffer.
// Encodes wchar_t (utf16le) to char in utf8.
//
// #define FILESYSTEM_USE_EXCEPTIONS to throw on unfixable errors.
// Defaults to truncating the Paths if they become too large.
//

namespace filesystem
{

class Path
{
public:

	enum {
		max_length = 260,
		slash = '\\',
	};

	typedef const Path& const_ref;

	//------------------------------------------------
	// Initialization
	//------------------------------------------------

	// Constructors
	Path();
	Path(const char* str);
	Path(const wchar_t* str);
	Path(const_ref rhs);
	// Assign
	Path& assign(const char* str);
	Path& assign(const wchar_t* str);
	Path& assign(const_ref rhs);
	Path& operator= (const char* str);
	Path& operator= (const wchar_t* str);
	Path& operator= (const_ref rhs);
	// Adds files and dirs
	Path& append(const char* str);
	Path& append(const wchar_t* str);
	Path& append(const_ref rhs);
	Path& operator/= (const char* str);
	Path& operator/= (const wchar_t* str);
	Path& operator/= (const_ref rhs);

	//------------------------------------------------
	// Modifiers
	//------------------------------------------------

	// Reset
	void clear();
	// Remove redundant slashes & converts to native format and verifies the contents to be a valid Path name
	bool normalize();
	// Expand environment vars, return value is false if one or more failed to expand
	// In which case it'll complete the job but the env var remains in % signs
	// WARNING! This function has a lot of edge cases you should be aware of! (probably best to avoid using it...)
	bool expand(char sign = '%');
	// Removes the filename
	void remove_filename();
	// Replaces the filename
	void replace_filename(const char* new_name);
	void replace_filename(const wchar_t* new_name);
	// Replaces the extension
	void replace_ext(const char* new_ext);
	void replace_ext(const wchar_t* new_ext);
	void replace_stem(const char* new_stem);
	void replace_stem(const wchar_t* new_stem);
	// Makes absolute
	void make_absolute(const_ref base);
	// Makes relative, error if we aren't a subdir of dir
	bool make_relative(const_ref base);
	// Ensures this Path is a directory by adding a trailing slash
	void make_dir();
	// Goes to parent Path, will also strip filename
	void parent_dir();

	//------------------------------------------------
	// Decomposition
	//------------------------------------------------

	// Gets the final extension (includes the .)
	const char* ext() const;
	char* ext();
	// Gets all extensions in case of multiple exts
	const char* stem() const;
	char* stem();
	// Gets the filename
	const char* filename() const;
	char* filename();
	// Gets the relative Path (NULL if such doesn't exist)
	// FIXME! Use .. notation on partial match? Requires a buffer to be passed though...
	const char* relative_path(const_ref base) const;
	char* relative_path(const_ref base);
	// Gets the Path without the root, must be an absolute Path
	const char* root_Path() const;
	char* root_Path();
	// Get the full thing
	const char* c_str() const;
	char* c_str();
	// Extracts as utf16le
	//void extract( wchar_t* buf, unsigned int size );

	//------------------------------------------------
	// Query
	//------------------------------------------------

	// Do we have anything
	bool empty() const;
	// Are we absolute: start with a drive letter
	bool is_absolute() const;
	// Are we relative: !is_absolute && !is_win_unc && !is_nt_devname
	bool is_relative() const;
	// Windows UNC Paths: begins with \\ or \\?\ -
	bool is_win_unc() const;
	// Windows NT Device Namespace: begins with \\.\ -
	bool is_nt_devname() const;
	// Are we a directory (checks for ending slash)
	bool is_directory() const;

	//------------------------------------------------
	// Iterating
	//------------------------------------------------



	static bool _unit_test();

protected:
	// Copy internal strings, assumes to is a ptr in the buffer
	char* _copy(char* to, const char* src);
	char* _copy(char* to, const wchar_t* src);

	// End of string (ptr to internal buffer)
	char* _end();

	// Error handling
	bool _testlen(const char* base, unsigned int chars);
	void _errlen();
	void _errenc(wchar_t c);

public:
	char buffer[max_length];
};


inline const_ref make_path(const char* str) { return *reinterpret_cast<const Path*>(str); }
inline Path make_path(const wchar_t* str) { return Path(str); }



// Inlines

inline Path::Path() { }
inline Path::Path(const char* str) { assign(str); }
inline Path::Path(const wchar_t* str) { assign(str); }
inline Path::Path(const_ref rhs) { assign(rhs); }
inline Path& Path::assign(const char* str) { _copy(buffer, str); return *this; }
inline Path& Path::assign(const wchar_t* str) { _copy(buffer, str); return *this; }
inline Path& Path::assign(const_ref rhs) { return assign(rhs.buffer); }
inline Path& Path::operator= (const char* str) { return assign(str); }
inline Path& Path::operator= (const wchar_t* str) { return assign(str); }
inline Path& Path::operator= (const_ref rhs) { return assign(rhs); }

inline Path& Path::append(const_ref rhs) { return append(rhs.buffer); }
inline Path& Path::operator/= (const char* str) { return append(str); }
inline Path& Path::operator/= (const wchar_t* str) { return append(str); }
inline Path& Path::operator/= (const_ref rhs) { return append(rhs.buffer); }

inline void Path::clear() { *(unsigned int*)buffer = 0; }

inline char* Path::ext() { return const_cast<char*>(const_cast<const Path*>(this)->ext()); }
inline char* Path::stem() { return const_cast<char*>(const_cast<const Path*>(this)->stem()); }
inline char* Path::filename() { return const_cast<char*>(const_cast<const Path*>(this)->filename()); }
inline char* Path::relative_path(const_ref base) { return const_cast<char*>(const_cast<const Path*>(this)->relative_path(base)); }
inline char* Path::root_Path() { return const_cast<char*>(const_cast<const Path*>(this)->root_Path()); }
inline const char* Path::c_str() const { return buffer; }
inline char* Path::c_str() { return buffer; }

inline bool Path::_testlen(const char* base, unsigned int chars)
{
	// Tests if we have room for additional characters starting from base
	if (static_cast<unsigned int>(base - buffer) >= (static_cast<unsigned int>(max_length) - chars - 1)) {
		_errlen();
		return false;
	}
	else {
		return true;
	}
}

}
