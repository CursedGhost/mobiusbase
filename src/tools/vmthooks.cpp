#include <cstdlib>
#include <cassert>
#include "vmthooks.h"

#ifndef container_of
#define container_of(ptr, type, member) ((type*)((char*)ptr - offsetof(type, member)))
#endif

namespace mobius
{

size_t CVMTManager::CountFuncs(vftable_t* vmt)
{
	// Q: Do vftables really end with a nullptr?
	// A: No they don't \(o.o)/
	// Q: Why do you pretend it is then?
	// A: Left as an excersise to the reader
	size_t i = ~0;
	do ++i; while ((*vmt)[i]);
	return i;

	// In all seriousness while the code above is technically incorrect it'll work
	// Here are some more alternatives and why they've not been chosen:

	// 1. VirtualQuery every vfunc and see if it lands in read+execute memory
	// Requires a VirtualQuery for every ptr which I don't feel like doing, otherwise this will be fine.

	// 2. Check the vtable until a vfunc lands outside the .text section of the containing vftable.
	// Requires knowing the module this vftable is in and parsing PE header.
	// Will break (miscount the vfuncs) when the vftable is already hooked by someone else, perhaps another cheat.
	// Not saying not doing this will guaranteed be fine, but there's a chance it will be fine :)

	// There's more to say like what if a vftable is hooked but the other guy hooking doesn't bother to put a nullptr at the end,
	// But at this point I don't really care.
}

CVMTManager::CVMTManager(void* inst, size_t offset, size_t vfuncs)
{
	_vftable = &_getvtbl(inst, offset);
	_oldvmt = *_vftable;
	// Count vfuncs ourself if needed
	if (!vfuncs) {
		vfuncs = CountFuncs(_oldvmt);
		assert(vfuncs > 0);
	}
	// Allocate room for the new vtable + 3 places before + 1 place after for a nullptr
	_table = (hktable_t*)::malloc((vfuncs + 4)*sizeof(uintptr_t));
	_table->self = this; // Ptr back to the hook object
	_table->guard = GUARD; // Guard tells us if the vtable is hooked
	_table->vftable[vfuncs] = nullptr; // Marks the end of the vtable
	// Copy over the other vfuncs + RTTI ptr
	{ size_t i = ~0; do _table->vftable[i] = (*_oldvmt)[i]; while (++i < vfuncs); }
}
CVMTManager::~CVMTManager()
{
	Unhook();
	::free(_table);
}
void CVMTManager::EraseHooks()
{
	size_t i = 0;
	do _table->vftable[i] = (*_oldvmt)[i];
	while (_table->vftable[++i]);
}
CVMTManager& CVMTManager::HookMethod(pfn_t newfunc, size_t index)
{
	assert(index<CountFuncs(&_table->vftable));
	_table->vftable[index] = newfunc;
	return *this;
}
CVMTManager& CVMTManager::UnhookMethod(size_t index)
{
	assert(index<CountFuncs(&_table->vftable));
	_table->vftable[index] = (*_oldvmt)[index];
	return *this;
}
bool CVMTManager::HookPresent(void* inst, size_t offset)
{
	hktable_t* tbl = container_of(_getvtbl(inst, offset), hktable_t, vftable);
	return tbl->guard == GUARD;
}
CVMTManager& CVMTManager::GetHook(void* inst, size_t offset)
{
	hktable_t* tbl = container_of(_getvtbl(inst, offset), hktable_t, vftable);
	return *tbl->self;
}
auto CVMTManager::_getvtbl(void* inst,size_t offset) -> vftable_t*&
{
	return *reinterpret_cast<vftable_t**>((char*)inst + offset);
}

}
