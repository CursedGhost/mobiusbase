#pragma once

//----------------------------------------------------------------
// Handle SEH exceptions as C++EH exceptions
//----------------------------------------------------------------
// Using this requires /EHa (C/C++ -> Code Generation -> Enable C++ Exceptions) and #define SEHGUARD_ENABLE

#include <stdexcept>

struct _EXCEPTION_POINTERS;

namespace mobius
{

// Guard will catch SE exceptions and translate to C++EH
class SehGuard
{
public:
	static void SehTranslator(unsigned int, _EXCEPTION_POINTERS*);
#ifdef SEHGUARD_ENABLE
	inline SehGuard()
	{
		_prev = _set_se_translator(&SehTranslator);
	}
	~SehGuard()
	{
		_set_se_translator(_prev);
	}
private:
	_se_translator_function _prev;
#endif
};

// C++ exception thrown when a SE exception is encountered
class seh_error : public std::runtime_error
{
public:
	seh_error(unsigned int code, _EXCEPTION_POINTERS* err);
	virtual const char* what() const;
private:
	unsigned int _code;
	_EXCEPTION_POINTERS* _err;
	mutable char _buf[32];
};

}
