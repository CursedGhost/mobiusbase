
#include "sehguard.h"
#include "printf.hpp"
#include <Windows.h>

namespace mobius
{

void SehGuard::SehTranslator( unsigned int code, _EXCEPTION_POINTERS* err )
{
	throw seh_error( code, err );
}
seh_error::seh_error(unsigned int code, _EXCEPTION_POINTERS* err) : std::runtime_error(""), _code(code), _err(err)
{
}
const char* seh_error::what() const
{
	cast_vabuf(_buf).printf("SEH 0x%08X @ 0x%08X", _code, _err->ExceptionRecord->ExceptionAddress);
	return _buf;
}

}
