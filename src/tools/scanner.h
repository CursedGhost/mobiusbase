#pragma once

//----------------------------------------------------------------
// Toolkit: Scanner
//----------------------------------------------------------------
// Deals with pattern scanning for signatures.

#include <Windows.h>
#include <stdint.h>

namespace mobius
{

//----------------------------------------------------------------
// Class: Scanner
//----------------------------------------------------------------
// Handles scanning in modules & executable files
class Scanner
{
public:
	// Some compile settings
	enum {
		/// Store buffer length, max number of '*' in pattern supported.
		MAX_STORE = 8,
		/// QuickSearch buffer length, max number of bytes used from the pattern in the prescan.
		QS_BUF_LEN = 15,
		/// Recursion buffer length, max number of recursions using '+'.
		MAX_DEPTH = 8,
	};

	typedef const uint8_t* ptr_t;

	/// Constructor to scan a module in memory in the specified section.
	/// \param hModule Module handle to scan in.
	/// \param section Section of the module to scan in, optional.
	explicit Scanner(HMODULE hModule, const char* section_name = nullptr);

	/// Keeps track of the scan state.
	struct State
	{
		/// Current pattern to match against.
		const char* pattern;
		/// QuickSearch optimization: buffer length.
		uint8_t qslen;
		/// QuickSearch optimization: bytes to match.
		uint8_t qsbuf[QS_BUF_LEN];
		/// Current scan pointer.
		ptr_t ptr;
		/// Store buffer.
		void* store[MAX_STORE];
		/// How many times the slow Match() code path was invoked.
		unsigned hits;
	};

	// Main scanner
	// Will throw an exception if the pattern is malformed. This is for your own good as the matcher doesn't do ANY checks on the pattern.
	//
	// Use case 'quick&easy':
	//  Invoke Scan with your pattern, access store in scan[index].
	//  Returns nullptr if not found or there are multiple matches.
	//
	// Use case 'adept':
	//  Scanner scan( ... );
	//  void* ptr;
	//  for ( scan.Setup( "pattern" ); ptr = scan.Next(); ; ) { use ptr and scan[index] }
	//
	// Note about the format of patterns:
	// Pattern uses human readable hex digits and several control characters, use spaces to make it easier to read:
	//  *   : Add pointer to this location to the store array.
	//  ?   : Ignore this byte (a single ? means 2 hex digits).
	//  ''  : Do a byte scan (text) on everything between quotes.
	//  j   : Absolute jump to the pointer stored here and continue scanning (4 or 8 byte pointers). Match will fail if pointer lands outside the bounds.
	//  rx  : Relative jump, x is either 1, 2, 4 or 8 denoting the size of the relative offset. Match will fail if pointer lands outside the bounds.
	//
	void* Scan(const char* pattern);
	void* operator() (const char* pattern);

	// Init the state for matching, throws on malformed patterns
	void Setup(const char* pattern);

	// Find the next match, only touches state if a match is found
	void* Next();

	// Matches pattern at current position
	bool Match(ptr_t pos);

	// Access the store array
	void* operator[] (size_t i) const;

	/// Validate patterns. Throws an std::exception if malformed.
	/// Ensures the pattern is valid allowing the main scanner to ignore safety checks for better performance.
	/// \param pattern The pattern to validate.
	static void Validate(const char* pattern);

private:
	// Scan range
	ptr_t _begin;
	ptr_t _end;
	// Range of memory allowed to touch
	ptr_t _lower;
	ptr_t _upper;
	// Scanner state
	State state;
};




inline void* Scanner::operator() (const char* pattern)
{
	return Scan(pattern);
}
inline void* Scanner::operator[] (unsigned i) const
{
	assert(i < MAX_STORE); return state.store[i];
}

}
