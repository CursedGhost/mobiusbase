#pragma once

//--------------------------------------------------------------------------------
// Toolkit: Tools
//--------------------------------------------------------------------------------
// Very basic memory tools

#include <cstddef>

namespace mobius
{

// getmember<T>(ptr, offset)
// Returns a reference as a type of your choice from an offset.

template<typename T>
inline T& getmember(void* ptr, intptr_t offset)
{
	return *reinterpret_cast<T*>((char*)ptr + offset);
}
template<typename T>
inline const T& getmember(const void* ptr, intptr_t offset)
{
	return *reinterpret_cast<const T*>((const char*)ptr + offset);
}



// make_ptr<T>(ptr, offset)
// Returns a pointer as a type of your choice.

template<typename T>
inline T make_ptr(void* ptr, intptr_t offset)
{
	return reinterpret_cast<T>((char*)ptr + offset);
}
template<typename T>
inline const T make_ptr(const void* ptr, intptr_t offset)
{
	return reinterpret_cast<const T>((const char*)ptr + offset);
}



// getvtable( inst, offset )
// Returns the vtable as an array of void pointers.

inline void**& getvtable(const void* inst, long offset = 0)
{
	return *reinterpret_cast<void***>((uintptr_t)inst + offset);
}



// getvfunc<T>(inst, index, offset)
// Returns the vfunc from the instance casted to a (function) type of your choice.

template<typename Fn>
inline Fn getvfunc(const void* inst, int index, intptr_t offset = 0)
{
	return *reinterpret_cast<Fn*>((char*)inst + offset)[index];
}

}
