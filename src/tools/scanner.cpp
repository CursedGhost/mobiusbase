#include <exception>
#include <cstdarg>
#include <cstdio>

#include "scanner.h"

namespace mobius
{

static uint8_t parsehex(char h)
{
	if (h >= '0' && h <= '9') {
		return static_cast<uint8_t>(h - '0');
	}
	else if (h >= 'a' && h <= 'f') {
		return static_cast<uint8_t>(h - 'a' + 10);
	}
	else if (h >= 'A' && h <= 'F') {
		return static_cast<uint8_t>(h - 'A' + 10);
	}
	return 0;
}
static bool ishex(char h)
{
	return parsehex(h) || h == '0';
}

Scanner::Scanner(HMODULE hModule, const char* section_name)
{
	// Find the size of this module
	auto nt_headers = (PIMAGE_NT_HEADERS)((uintptr_t)hModule + ((PIMAGE_DOS_HEADER)hModule)->e_lfanew);
	_lower = (ptr_t)hModule;
	_upper = _lower + nt_headers->OptionalHeader.SizeOfImage;

	// Set the scan limits
	_begin = _lower;
	_end = _upper;
	if (section_name) {
		for (
			auto it = (PIMAGE_SECTION_HEADER)((uintptr_t)&nt_headers->OptionalHeader + nt_headers->FileHeader.SizeOfOptionalHeader),
			end = it + nt_headers->FileHeader.NumberOfSections;
			it < end;
			++it) {
			if (!strncmp((char*)it->Name, section_name, IMAGE_SIZEOF_SHORT_NAME)) {
				_begin = _lower + it->VirtualAddress;
				_end = _begin + it->Misc.VirtualSize;
				break;
			}
		}
	}
}

void* Scanner::Scan(const char* pattern)
{
	// Setup the state
	Setup(pattern);
	// Find a match
	void* ptr;
	if ((ptr = Next())) {
		// Only allow 1 unique match
		if (Next()) {
			ptr = nullptr;
		}
		// Rematch to update the state
		else {
			Match((ptr_t)ptr);
		}
	}
	return ptr;
}
void Scanner::Setup(const char* pattern)
{
	state.pattern = pattern;
	state.ptr = _begin - 1;
	state.hits = 0;
	// One time, one day, you'll love me for this.
	Validate(pattern);
	// Optimize the query with quicksearch
	state.qslen = 0;
	for (const char* it = state.pattern; state.qslen < QS_BUF_LEN; ++it) {
		char c = *it;
		uint8_t b;

		if ((b = parsehex(c)) || c == '0') {
			// Get the 2nd (low) nibble
			uint8_t lo = parsehex(*++it);
			// Compose and store
			state.qsbuf[state.qslen++] = (b << 4) | lo;
		}
		else if (c != ' ' && c != '*') {
			// Cannot quicksearch beyond these
			break;
		}
	}
}
void* Scanner::Next()
{
	// Strategy:
	//  Cannot optimize the search, just brute-force it.
	if (state.qslen == 0) {
		for (ptr_t ptr = state.ptr + 1; ptr < _end; ++ptr) {
			if (Match(ptr)) {
				return (void*)ptr;
			}
		}
	}
	// Strategy:
	//  Raw pattern is too small for full blown quicksearch.
	//  Can still do a small optimization though.
	else if (state.qslen <= 2) {
		uint8_t c = state.qsbuf[0];
		for (ptr_t ptr = state.ptr + 1; ptr < _end; ++ptr) {
			if (ptr[0] == c && Match(ptr)) {
				return (void*)ptr;
			}
		}
	}
	// Strategy:
	//  Full blown quicksearch for raw pattern.
	//  Most likely completely uneccessary but oh well...
	else {
		uint8_t len = state.qslen;

		// Initialize the jump table for quicksearch
		uint8_t jumps[256];
		memset(jumps, len, sizeof(jumps));
		len -= 1;
		for (uint8_t i = 0; i < len; ++i) jumps[state.qsbuf[i]] = len - i;

		// Adjust the end pointer
		ptr_t end = _end - len;

		// Loop trough the memory
		for (ptr_t p = state.ptr + 1; p < end; p += jumps[*(p + len)]) {
			if (*p != state.qsbuf[0]) {
				continue;
			}
			// Check the rest
			for (unsigned i = 1; i < len; ++i) {
				if (p[i] != state.qsbuf[i]) {
					goto mismatch;
				}
			}
			// Found a match, check full pattern
			if (Match(p)) {
				return (void*)p;
			}
		mismatch:;
		}
	}
	// Not found
	return nullptr;
}
bool Scanner::Match(ptr_t start)
{
	++state.hits;
	ptr_t pos = start;
	// Assumes everything is fine.
	ptr_t stack[MAX_DEPTH];
	unsigned stacki = 0;
	bool recurse = false;
	unsigned sti = 0;
	for (const char* it = state.pattern; *it; ++it) {
		char c = *it;
		switch (c) {
		case '*':
			// Save this position
			state.store[sti++] = (void*)pos;
			break;
		case '?':
			// Ignore this value
			++pos;
			break;
		case '+':
			// Recurse on next jump
			recurse = true;
			break;
		case '-':
			// Return from recursion
			pos = stack[--stacki];
			break;
		case 'j': case 'J':
			// Absolute jump
			if (recurse) {
				stack[stacki++] = pos + sizeof(void*);
				recurse = false;
			}
			pos = *(ptr_t*)pos;
			break;
		case 'r': case 'R':
			// Relative jump
			// Read offset size
			c = *++it - '0';
			// Store in case of recursion
			if (recurse) {
				stack[stacki++] = pos + c;
				recurse = false;
			}
			// Read offset and sign extend it
			int offset;
			if (c == 1) offset = *(char*)pos;
			else if (c == 2) offset = *(short*)pos;
			else if (c == 4) offset = *(int*)pos;
			// FIXME! qword pointers on 64bit systems!
			pos = pos + offset + c;
			break;
		case '0': case '1': case '2': case '3':
		case '4': case '5': case '6': case '7':
		case '8': case '9': case 'A': case 'B':
		case 'C': case 'D': case 'E': case 'F':
		case 'a': case 'b': case 'c': case 'd':
		case 'e': case 'f':
		{
			// Read a hex digit
			// Doesn't care about errors
			uint8_t c = (parsehex(it[0]) << 4) | parsehex(it[1]);
			if (*pos != c) {
				return false;
			}
			++it, ++pos;
		}
		break;
		case '\'': case '\"':
			// Raw comparison
			for (char e = *it++; *it != e; ++it, ++pos)
			{
				uint8_t b = static_cast<uint8_t>(*it);
				if (*pos != b) {
					return false;
				}
			}
			break;
		default:
			// Ignore any other characters
			break;
		}
		// Make sure the scan pointer is still within the bounds
		// FIXME! This check disallows matching at the edge of the scan range!
		if (pos < _lower || pos >= _upper) {
			return false;
		}
	}

	state.ptr = start;
	return true;
}







void Scanner_Format(char* buf, unsigned len, const char* fmt, ...)
{
	va_list va;
	va_start(va, fmt);
	::vsnprintf(buf, len, fmt, va);
	va_end(va);
}
void Scanner::Validate(const char* pattern)
{
	enum reason_t {
		SUCCESS = 0,
		UNEXPECTED_EOS,			// Expecting more input
		INVALID_RECURSION,		// Found a '+' not immediately followed by a jump
		INVALID_SIZEOF,			// Sizeof for relative jump is not either '1', '2' or '4'
		UNPAIRED_HEXDIGIT,		// A hex digit was not followed by another one
		UNKNOWN_CHARACTER,		// Only accepts spaces to make the pattern more friendly to read. All other characters are reserved for future use
		STACK_ERROR,			// Too much or invalid use of '+' and '-'
		UNKNOWN_ESCAPE,			// Only accepts escape sequences \\ \0 \n \t \' and \"
		STORE_OVERFLOW,			// Too many stores
	};
	static const char* reason_to_str[] = {
		"Success",
		"Unexpected EOS",
		"Invalid recursion",
		"Invalid sizeof",
		"Unpaired hexdigit",
		"Unknown character",
		"Stack error",
		"Unknown escape",
		"Store overflow"
	};
	reason_t reason;
	const char* it;

#define fail(r) do { reason = r; goto fail; } while ( false )
	{
		int rec = 0;
		int store = 0;
		// This validator has some very strict rules
		for (it = pattern; *it; ++it) {
			switch (*it) {
			case '+':
			{
				// Recursive sign must be followed by a jump
				char n = *(it + 1);
				rec++;
				if (rec > MAX_DEPTH) fail(STACK_ERROR);
				if (!n) fail(UNEXPECTED_EOS);
				if (n == 'j' || n == 'J' || n == 'r' || n == 'R') break;
				fail(INVALID_RECURSION);
			}
			break;
			case 'r': case 'R':
			{
				// Relative jump must be followed by a 1, 2, 4 or 8 (in a 64 bit system)
				char n = *++it;
				if (!n) fail(UNEXPECTED_EOS);
				if (n == '1' || n == '2' || n == '4') break;
				fail(INVALID_SIZEOF);
			}
			break;
			case '0': case '1': case '2': case '3':
			case '4': case '5': case '6': case '7':
			case '8': case '9': case 'A': case 'B':
			case 'C': case 'D': case 'E': case 'F':
			case 'a': case 'b': case 'c': case 'd':
			case 'e': case 'f':
			{
				// Hex digits must be paired
				char n = *++it;
				if (!n) fail(UNEXPECTED_EOS);
				if (!ishex(n)) fail(UNPAIRED_HEXDIGIT);
			}
			break;
			case '-':
				// Check recursion
				if (--rec < 0) fail(STACK_ERROR);
				break;
			case '\'': case '\"':
			{
				// Check raw comparison
				char e = *it;
				if (!(*++it)) fail(UNEXPECTED_EOS);
				for (; *it != e; it++)
				{
					char c = *it;
					if (!c) fail(UNEXPECTED_EOS);
				}
			}
			break;
			case '*':
				// Only allow 8 of these
				if (++store > MAX_STORE) fail(STORE_OVERFLOW);
				break;
			case ' ': case '?': case 'j': case 'J':
				// Other allowed characters:
				break;
			default:
				// The rest is not allowed
				fail(UNKNOWN_CHARACTER);
			}
		}
		if (rec) fail(STACK_ERROR);
	}
#undef fail

	// SUCCESS!
	return;

	// FAIL!:(
fail:
	{
		char buf[512];
		Scanner_Format(buf, sizeof(buf), "Malformed (%d) @%d in <%s>", reason, it - pattern, pattern);
		throw std::exception(buf);
	}
}

}
