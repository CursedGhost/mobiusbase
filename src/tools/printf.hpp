#pragma once

//----------------------------------------------------------------
// Printf helper tools
//----------------------------------------------------------------

#include <cstdarg>

namespace mobius
{

//----------------------------------------------------------------
// Helper to select right version of vsn(w)printf and _vsc(w)printf

/// Templated version of vsn(w)printf.
/// \tparam T Character type.
/// \param buf[out] Pointer to a buffer where the resulting formatted string is stored. The buffer should have a size of at least count characters.
/// \param count[in] Maximum number of characters to be used in the buffer.
/// \param fmt[in] Format string.
/// \param va[in] Variable arguments.
/// \return Number of characters printed.
template<typename T> int vsnprintf(T* buf, size_t count, const T* fmt, va_list va);

/// Templated version of vsc(w)printf.
/// \tparam T Character type.
/// \param fmt[in] Format string.
/// \param va[in] Variable arguments.
/// \return Length of the formatted string in characters.
template<typename T> int vscprintf(const T* fmt, va_list va);


//----------------------------------------------------------------
// Class va_buf
//
// Create temporary buffers with formatted data.
//
// Some issues:
// * Generates code for pretty much every unique template instance, just accept it.
// * Gets passed by value to a function taking variable arguments (pass the address instead!).
// * Fails with auto-deduced string argument accepting chars or wide chars (explicitly specify char type for the template).
//
// WARNING!
//
// Do not pass a va_buf instance by value to a function as a variable arg!
// Instead, make sure to take the address before passing it.
// It will work as expected (and auto-cast to char/wchar_t* as required) everywhere else.
//
// Example, will fail:  printf( "%s\n", va_printf<64>( "Hello %s!", "World" );
// Do this instead:     printf( "%s\n", &va_printf<64>( "Hello %s!", "World" );

template<size_t L, typename T>
class va_buf
{
	typedef typename T buffer_t[L];
	typedef typename const T const_buffer_t[L];

	buffer_t buf;

public:
	/// Default constructor, the buffer is left fully uninitialized.
	va_buf();

	/// Zero constructor, the buffer is initialized to a zero length string.
	explicit va_buf(int);

	/// Formatting constructor, see va_printf for auto type deduction.
	/// \param fmt Formatting string.
	/// \param ... Formatting arguments.
	explicit va_buf(const T* fmt, ...);

	/// Formatting function from variadic arguments.
	/// \param fmt Formatting string.
	/// \param ... Formatting arguments.
	/// \return Number of characters printed.
	int printf(const T* fmt, ...);

	/// Formatting function.
	/// \param fmt Formatting string.
	/// \param va Formatting arguments.
	/// \return Number of characters printed.
	int vprintf(const T* fmt, va_list va);

	/// Assign a plain string.
	/// \param str String to copy.
	/// \return Number of characters copied.
	int assign(const T* str);

public:
	// Assignment convenience operator.
	inline va_buf<L, T>& operator= (const T* str) { assign(str); return *this; }
	// Make this class transparently behave as if it's an array.
	inline operator buffer_t& () { return buf; }
	// Ambiguity Errors Ahead! The above should be good enough as long as you don't const va_buf instances.
	//inline operator const_buffer_t& () const { return buf; }
};

/// \tparam L Buffer size of the array. Should be automatically deduced from raw.
/// \tparam T Character type of the buffer.
/// \param array 
/// \return The input reinterpret_cast'ed to va_buf type.
template<size_t L, typename T>
va_buf<L, T>& cast_vabuf(T (&array)[L]);

/// Format a string and return the result by value.
/// \tparam L Buffer size of the underlying array.
/// \tparam T Character type of the buffer.
/// \param fmt Format string.
/// \param ... Format arguments.
/// \return Buffer with formatted string.
template<size_t L, typename T>
inline va_buf<L, T> va_printf(const T* fmt, ...);

}



#include <cassert>
#include <cstdio>

namespace mobius
{

#ifdef _MSC_VER
// Fixes requirement for _CRT_SECURE_NO_WARNINGS
// i make no promises that what i do is safe
#pragma warning(push)
#pragma warning(disable:4996)
template<> inline int vsnprintf<char>(char* buf, size_t count, const char* fmt, va_list va) {
	int n = ::vsnprintf(buf, count, fmt, va);
	// Standard says to return the number of characters written as if the buffer is always large enough.
	// Windows/MSVC of course, return -1 if output is truncated...
	if (/*n < 0 || */static_cast<size_t>(n) >= count) {
		buf[count - 1] = '\0'; // Because I care about null termination...
		n = static_cast<int>(count); // Still not correct but I don't care enough...
	}
	return n;
}
template<> inline int vsnprintf<wchar_t>(wchar_t* buf, size_t count, const wchar_t* fmt, va_list va) {
	int n = ::_vsnwprintf(buf, count, fmt, va);
	if (/*n < 0 || */static_cast<size_t>(n) >= count) {
		buf[count - 1] = '\0';
		n = static_cast<int>(count);
	}
	return n;
}
template<> inline int vscprintf<char>(const char* fmt, va_list va) {
	return ::_vscprintf(fmt, va);
}
template<> inline int vscprintf<wchar_t>(const wchar_t* fmt, va_list va) {
	return ::_vscwprintf(fmt, va);
}
#pragma warning(pop)
#endif // _MSC_VER

template<size_t L, typename T>
inline va_buf<L, T>::va_buf() {}
template<size_t L, typename T>
inline va_buf<L, T>::va_buf(int) {
	buf[0] = 0;
}
template<size_t L, typename T>
inline va_buf<L, T>::va_buf(const T* fmt, ...) {
	va_list va;
	va_start(va, fmt);
	vprintf(fmt, va);
}
template<size_t L, typename T>
inline int va_buf<L, T>::printf(const T* fmt, ...) {
	va_list va;
	va_start(va, fmt);
	return vprintf(fmt, va);
}
template<size_t L, typename T>
inline int va_buf<L, T>::vprintf(const T* fmt, va_list va) {
	return vsnprintf<T>(buf, L, fmt, va);
}
template<size_t L, typename T>
inline int va_buf<L, T>::assign(const T* str) {
	// Dumb copy which fills the buffer and cuts off if it doesn't fit
	for (size_t i = 0; i < L; ++i) {
		buf[i] = str[i];
		if (!str[i]) {
			return static_cast<int>(i);
		}
	}
	buf[L - 1] = 0;
	return static_cast<int>(L);
}

template<size_t L, typename T>
inline va_buf<L, T>& cast_vabuf(T(&array)[L]) {
	return reinterpret_cast<va_buf<L, T>&>(array);
}

template<size_t L, typename T>
inline va_buf<L, T> va_printf(const T* fmt, ...) {
	va_buf<L, T> buf;
	va_list va;
	va_start(va, fmt);
	buf.vprintf(fmt, va);
	return buf;
}

}
