#include "stdafx.h"

#include "cheatmgr.h"

namespace mobius
{

CCheatManager::CCheatManager() : cheats(nullptr), callbacks(nullptr)
{
}
CCheatManager::~CCheatManager()
{
	// Clean up the cheats
	for (auto it = cheats; it; it = it->next) {
		delete it;
	}
}
void CCheatManager::Init(CMobiusBase* base)
{
	ICheat::init_t init = { this, base };

	// For each stored cheat
	for (auto it = cheats; it; it = it->next) {
		it->Init(init);
	}
}
void CCheatManager::Shutdown()
{
}

// Management

void CCheatManager::Crashed(callback_t& cb, const std::exception& err)
{
	// Warning! If we crash or throw an exception here the game will terminate!
}
void CCheatManager::Register(ICheat* ch)
{
	ch->next = cheats;
	cheats = ch;
}
void CCheatManager::AddCallback(callback_t& cb)
{
	cb.next = callbacks;
	callbacks = &cb;
}
void CCheatManager::Filter(filtered_t& out, cbt_t type)
{
	out.clear();

	// List all matching callbacks
	for (auto it = callbacks; it; it = it->next) {
		if (it->type == type) {
			out.push_back(it);
		}
	}

	// Sort by priority (yay bubble sort)
	if (out.size() > 1) {
		for (unsigned int c = 0, end = out.size() - 1; c < end; ++c) {
			for (unsigned int i = 0; i < (end - c); ++i) {
				if (out[i]->priority > out[i + 1]->priority) {
					std::swap(out[i], out[i + 1]);
				}
			}
		}
	}
}
ICheat* CCheatManager::Lookup(const char* id)
{
	for (auto it = cheats; it; it = it->next) {
		const char* name = it->Name();
		if (!strcmp(name, id)) {
			return it;
		}
	}
	return nullptr;
}

// Callbacks

void CCheatManager::CreateMove(createmove_t& cm)
{
	const QAngle va = cm.pCommand->viewangles;

	CTickVars vars(this, cm);
	PredictMove(vars);

	vars.Fix(va);
}
void CCheatManager::PredictMove(CTickVars& vars)
{
	Invoke(CBT_TICK, vars);
}
bool CCheatManager::Update()
{
	// Only update when ingame
	if (Connect(gpGame->GetSignonState())) {
		CUpdateVars vars(this);
		Invoke(CBT_UPDATE, vars);
		return true;
	}

	return false;
}
void CCheatManager::Paint(CViewSetup& view)
{
	CPaintVars vars(this);
	Invoke(CBT_PAINT, vars);
}
void CCheatManager::ViewSetup(CViewSetup& view)
{

	Invoke(CBT_VIEW, vars);
}
bool CCheatManager::Connect(SignonState state)
{
	if (signon_state != state) {
		signon_state = state;

		CConnectVars vars(this, state);
		Invoke(CBT_CONNECT, vars);
	}
	return state == SIGNONSTATE_FULL;
}
action_t CCheatManager::GameEvent(IGameEvent* evt)
{
	CEventVars vars(this, evt);
	Invoke(CBT_EVENT, vars);
	return vars.action;
}
action_t CCheatManager::UserMessage(usermsg_t& msg)
{
	CMessageVars vars(this, msg);
	Invoke(CBT_MESSAGE, vars);
	return vars.action;
}
action_t CCheatManager::EmitSound(emitsound_t& snd)
{
	CSoundVars vars(this, snd);
	Invoke(CBT_SOUND, vars);
	return vars.action;
}

void CCheatManager::Invoke(cbt_t type, CBaseVars& vars)
{
	// Get filtered and sorted list of callbacks
	filtered_t out;
	Filter(out, type);

	// Invoke the callbacks
	for (auto it = out.begin(), end = out.end(); it != end; ++it) {
		callback_t& cb = **it;
		// Eat crashes for convenience
		try {
			cb.callback(cb.inst, vars);
			if (vars.action != ACTION_CONTINUE) {
				break;
			}
		}
		catch (const std::exception& err) {
			Crashed(cb, err);
		}
	}
}

}
