#pragma once

#include "../ifaces.h"

namespace mobius
{
class CTF2Ifaces : public IGameIfaces
{
public:
	virtual void Init();
	
	// ---- Engine Funcs ----

	virtual void NET_SetConVar(ConVar* cvar, const char* value) override;
	virtual bool GetPlayerInfo(int player, player_info_t& pinfo) override;
	virtual int GetLocalPlayer() override;
	virtual int GetPlayerForUserID(int userid) override;
	virtual void GetViewAngles(QAngle& va) override;
	virtual void SetViewAngles(const QAngle& va) override;
	virtual SignonState GetSignonState() override;
	virtual const char* GetLevelName() override;
	virtual INetChannel* GetNetChannel() override;
	virtual void ConNPrintf(int pos, char* str) override;
	virtual void ClientCmd(const char* cmd) override;
	virtual void ServerCmd(const char* cmd) override;

	// --- Client funcs ---

	virtual ClientClass* GetAllClasses() override;
	virtual int PseudoRandom(int seed) override;

	virtual int SharedRandomInt(int randomSeed, const char* sharedName, int minVal, int maxVal, int baseSeed = 0) override;
	virtual float SharedRandomFloat(int randomSeed, const char* sharedName, float minVal, float maxVal, int baseSeed = 0) override;

	// --- Cvar funcs ---

	virtual alias_t* FindAlias( const char* name );
	virtual ConCommandBase* FindCvar( const char* name );

private:
	unsigned char* pvNetSetVar;
};
}
