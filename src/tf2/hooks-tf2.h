#pragma once

#include "../hooks.h"
#include "ifaces-tf2.h"

namespace mobius
{

class CTF2Cheats;

class CTF2Hooks : public CHookManager
{
public:
	CTF2Hooks(IGameIfaces* game, CTF2Cheats* cheats);

	virtual void Rehook() override;
	virtual void Unhook() override;

	// ClientMode hooks

	static bool __fastcall HookedCreateMove(IClientMode* self, int edx, float frametime, CUserCmd* pUserCmd);

protected:
	CTF2Cheats* cheats;

	hook_t hookShared;
};

}
