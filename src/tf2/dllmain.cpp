// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#include "mobius-tf2.h"
#include "ifaces-tf2.h"

inline bool GetKey(int vKey)
{
	ButtonCode_t code = mobius::gpGame->pInputSystem->VirtualKeyToButtonCode(vKey);
	return mobius::gpGame->pInputSystem->IsButtonDown(code);
	//return (GetAsyncKeyState(vKey)&(1 << 15)) != 0;
}

DWORD WINAPI LoaderThread(LPVOID param)
{
	// Actually load crap
	if (!LoadHack()) {
		// Keep this thread alive for as long as the hack is loaded
		// This provides a nice and easy way to unload on a hotkey
		// Allowing super fast iteration.
		while (!(GetKey(VK_LSHIFT) && GetKey(VK_ADD))) {
			Sleep(10);
		}
	}
	// Cleanup resources
	SignalExit();
	// Unload the DLL
	HMODULE hmCheat = (HMODULE)param;
	FreeLibraryAndExitThread(hmCheat, 0);
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD dwReason, LPVOID lpReserved)
{
	if (dwReason == DLL_PROCESS_ATTACH) {
		// Spawn a thread to initialize ourselves
		::CreateThread(nullptr, 0, &LoaderThread, hModule, 0, nullptr);
	}
	return TRUE;
}
