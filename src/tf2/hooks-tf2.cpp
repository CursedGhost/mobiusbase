#include "stdafx.h"

#include "hooks-tf2.h"
#include "cheats-tf2.h"
#include "../shared.h"

namespace mobius
{


CTF2Hooks::CTF2Hooks(IGameIfaces* game, CTF2Cheats* cheats) : cheats(cheats),
	hookShared(*game->ppClientMode)
{
	// ClientModeShared hooks
	hookShared.SetUserData(this);
	hookShared.HookMethod(&HookedCreateMove, 21);
}
void CTF2Hooks::Rehook()
{
	hookShared.Rehook();
}
void CTF2Hooks::Unhook()
{
	hookShared.Unhook();
}

bool __fastcall CTF2Hooks::HookedCreateMove(IClientMode* self, int edx, float frametime, CUserCmd* ucmd)
{
	HookBegin();

	if (ucmd->command_number > 0) {
		createmove_t cm = { ucmd, ucmd->command_number, frametime };

		// Get our context
		hook_t& hook = hook_t::GetHook(self);
		auto cheats = hook.GetUserData<CTF2Hooks>()->cheats;

		// Call original
		typedef bool(__thiscall* CreateMoveFn)(IClientMode*, float, CUserCmd*);
		if (hook.GetMethod<CreateMoveFn>(21)(self, frametime, ucmd)) {
			gpGame->SetViewAngles(ucmd->viewangles);
		}

		// Invoke callbacks
		cheats->CreateMove(cm);
	}

	return HookEnd(), false;
}

}
