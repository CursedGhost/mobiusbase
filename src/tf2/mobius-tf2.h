#pragma once

#include "..\mobius.h"

namespace mobius
{

class CMobiusTF2 : public CMobiusBase
{
public:
	typedef CMobiusBase BaseClass;
	virtual int InitHack();

	static CMobiusTF2* g;
};

}
