#include "stdafx.h"

#include "../tools/scanner.h"

#include "ifaces-tf2.h"

namespace mobius
{

void CTF2Ifaces::Init()
{
	Scanner
		scanEngine(hmEngine, ".text"),
		scanClient(hmClient, ".text");

	//--------------------------------
	// CreateInterfaceFns
	//--------------------------------

	pfnClient = (CreateInterfaceFn) ::GetProcAddress(hmClient, "CreateInterface");

	pfnAppSystem = nullptr; // Right after "SERVERPLUGINCALLBACKS003"
	if (scanEngine("8B11 8B12 83C404 50 A1*???? 50 FFD2 84C0 75"))
		pfnAppSystem = **(CreateInterfaceFn**)scanEngine[0];

	//--------------------------------
	// Interfaces
	//--------------------------------

	pEngine = (IVEngineClient*)pfnAppSystem("VEngineClient013", nullptr);
	pEvents = (IGameEventManager2*)pfnAppSystem("GAMEEVENTSMANAGER002", nullptr);
	pTrace = (IEngineTrace*)pfnAppSystem("EngineTraceClient003", nullptr);
	pSound = (IEngineSound*)pfnAppSystem("IEngineSoundClient003", nullptr);
	pVGUI = (IEngineVGui*)pfnAppSystem("VEngineVGui001", nullptr);
	pModelInfo = (IVModelInfoClient*)pfnAppSystem("VModelInfoClient006", nullptr);
	pModelRender = (IVModelRender*)pfnAppSystem("VEngineModel016", nullptr);
	pOverlay = (IVDebugOverlay*)pfnAppSystem("VDebugOverlay003", nullptr);
	pRenderView = (IVRenderView*)pfnAppSystem("VEngineRenderView014", nullptr);
	pStudioRender = (IStudioRender*)pfnAppSystem("VStudioRender025", nullptr);
	pMaterial = (IMaterialSystem*)pfnAppSystem("VMaterialSystem080", nullptr);

	pCvar = (ICvar*)pfnAppSystem("VEngineCvar004", nullptr);

	pClient = (IBaseClientDLL*)pfnClient("VClient017", nullptr);
	pEntityList = (IClientEntityList*)pfnClient("VClientEntityList003", nullptr);
	pTools = (IClientTools*)pfnClient("VCLIENTTOOLS001", nullptr);
	pGameMovement = (IGameMovement*)pfnClient("GameMovement001", nullptr);
	pPrediction = (IPrediction*)pfnClient("VClientPrediction001", nullptr);

	pSurface = (vgui::ISurface*)		pfnAppSystem("VGUI_Surface030", nullptr);
	pPanels = (vgui::IPanel*)		pfnAppSystem("VGUI_Panel009", nullptr);
	pInputSystem = (IInputSystem*)pfnAppSystem("InputSystemVersion001", nullptr);

	//--------------------------------
	// Manual stuff
	//--------------------------------

	pGlobals = **make_ptr<CGlobalVarsBase***>(getvtable(pClient)[0], 0x60); // pClient->HudUpdate
	pInput = **make_ptr<IInput***>(getvtable(pClient)[21], 0x28); // pClient->CreateMove
	pUserMessages = **make_ptr<CUserMessages***>(getvtable(pClient)[36], 0x5); // pClient->DispatchUserMessage

	pAchMgr = getvfunc<IAchievementMgr* (__thiscall*)(IVEngineClient*)>(pEngine, 0x1C8 / 4)(pEngine);

	ppGameRes = nullptr;
	if (pfnGameResources = (GameResourcesFn)scanClient("A1*???? 85 C0 74 06 05"))
		ppGameRes = *(IClientEntity***)scanClient[0];

	pClState = nullptr; // "Playing demo from %s.\n", next to "DEMO"
	if (scanEngine("8B510C 52 6A00 68*"))
		pClState = *(CClientState**)scanEngine[0];

	ppClientMode = nullptr; // before "scripts/vgui_screens.txt"
	if (void* p = scanClient("83C404 C705*????*???? E8???? 8B10 8BC8 8B02 68"))
		ppClientMode = *(IClientMode***)scanClient[0];

	pvNetSetVar = (unsigned char*)scanEngine("56 57 8D4DD4 E8???? 8B0D");

	pfnSharedRandomInt = (SharedRandomIntFn)scanClient("55 8BEC 83EC08 8B4514 8B0D????");
	//pfnSharedRandomFloat	= (SharedRandomFloatFn)	scanClient( "" );

	pfnMD5_PseudoRandom = (MD5_PseudoRandomFn)scanClient("55 8BEC 83EC68 6A");
}
void CTF2Ifaces::NET_SetConVar(ConVar* cvar, const char* value)
{

}
bool CTF2Ifaces::GetPlayerInfo(int player, player_info_t& pinfo)
{
	return pEngine->GetPlayerInfo(player, &pinfo);
}
int CTF2Ifaces::GetLocalPlayer()
{
	return pEngine->GetLocalPlayer();
}
int CTF2Ifaces::GetPlayerForUserID(int userid)
{
	return pEngine->GetPlayerForUserID(userid);
}
void CTF2Ifaces::GetViewAngles(QAngle& va)
{
	pEngine->GetViewAngles(va);
}
void CTF2Ifaces::SetViewAngles(const QAngle& va)
{
	pEngine->SetViewAngles(va);
}
SignonState CTF2Ifaces::GetSignonState()
{
	// IsInGame +68
	//bool bIsInGame = toolkit::getvfunc<bool (__thiscall*)( IVEngineClient* )>( pEngine, 0x68/4 )( pEngine );
	// IsConnected +6C
	//bool bIsConnected = toolkit::getvfunc<bool (__thiscall*)( IVEngineClient* )>( pEngine, 0x6C/4 )( pEngine );
	typedef bool (__thiscall* IsConnectedFn)(IVEngineClient*);

	/* CEngineClient::IsInGame()
	539B4C90 33 C0                xor         eax,eax
	539B4C92 83 3D 98 C7 D6 53 06 cmp         dword ptr ds:[53D6C798h],6
	539B4C99 0F 94 C0             sete        al
	539B4C9C C3                   ret
	*/
	auto pSignonState = getmember<SignonState*>(getvtable(pEngine)[0x68 / 4], 0x4);
	return *pSignonState;
}
const char* CTF2Ifaces::GetLevelName()
{
	return pEngine->GetLevelName();
}
INetChannel* CTF2Ifaces::GetNetChannel()
{
	return static_cast<INetChannel*>(pEngine->GetNetChannelInfo());
}
void CTF2Ifaces::ConNPrintf(int pos, char* str)
{
	// Con_NPrintf +74
	// Con_NXPrintf +78
	char* next;
	do {
		if ((next = strchr(str, '\n'))) {
			*next = 0;
		}
		pEngine->Con_NPrintf(pos++, "%s", str);
		str = next + 1;
	} while (next);
}
void CTF2Ifaces::ClientCmd(const char* cmd)
{
	pEngine->ClientCmd_Unrestricted(cmd);
}
void CTF2Ifaces::ServerCmd(const char* cmd)
{
	pEngine->ServerCmd(cmd);
}
ClientClass* CTF2Ifaces::GetAllClasses()
{
	return pClient->GetAllClasses();
}
int CTF2Ifaces::PseudoRandom(int seed)
{
	return static_cast<int>(pfnMD5_PseudoRandom(static_cast<int>(seed)) & 0x7FFFFFFF);
}
int CTF2Ifaces::SharedRandomInt(int randomSeed, const char* sharedName, int minVal, int maxVal, int baseSeed)
{
	*getmember<int*>((void*)pfnSharedRandomInt, 0xB) = randomSeed;
	return pfnSharedRandomInt(sharedName, minVal, maxVal, baseSeed);
}
float CTF2Ifaces::SharedRandomFloat(int randomSeed, const char* sharedName, float minVal, float maxVal, int baseSeed)
{
	return pfnSharedRandomFloat(sharedName, minVal, maxVal, baseSeed);
}
alias_t* CTF2Ifaces::FindAlias(const char* name)
{
	return nullptr;
}
ConCommandBase* CTF2Ifaces::FindCvar(const char* name)
{
	return pCvar->FindVar(name);
}

}
