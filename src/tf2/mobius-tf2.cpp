#include "stdafx.h"

#include "mobius-tf2.h"
#include "cheats-tf2.h"
#include "ifaces-tf2.h"

namespace mobius
{

CMobiusTF2* CMobiusTF2::g = nullptr;

int CMobiusTF2::InitHack()
{
	// Instance the Game Interface
	game = new CTF2Ifaces();
	game->Init();

	// Spawn render & GUI systems

	// Create the cheat resources
	cheats = new CTF2Cheats();

	// Load resources

	// Attach hooks

	return BaseClass::InitHack();
}

}

// Wrappers

MOBIUSAPI int MOBIUSCALL LoadHack()
{
	mobius::CMobiusTF2*& base = mobius::CMobiusTF2::g;
	if ( base = new mobius::CMobiusTF2() )
	{
		return base->LoadHack();
	}
	return 3;
}
MOBIUSAPI void MOBIUSCALL SignalExit()
{
	mobius::CMobiusTF2*& base = mobius::CMobiusTF2::g;
	base->ShutDown();
	delete base;
	base = nullptr;
}

