#ifndef HGUARD_SDK_VECTOR
#define HGUARD_SDK_VECTOR

#include <cmath>


class Vector;
class QAngle;

class Vector
{
public:
	Vector() { }
	Vector( float xyz ) : x(xyz), y(xyz), z(xyz) { }
	Vector( float x, float y, float z ) { this->x = x; this->y = y; this->z = z; }
	Vector( const Vector& vec ) : x(vec.x), y(vec.y), z(vec.z) { }
	Vector( float* data ) : x(data[0]), y(data[1]), z(data[2]) { }

	inline Vector& Add( const Vector& vec ) { x+=vec.x; y+=vec.y; z+=vec.z; return *this; }
	inline Vector& Sub( const Vector& vec ) { x-=vec.x; y-=vec.y; z-=vec.z; return *this; }
	inline Vector& Mult( float fl ) { x*=fl; y*=fl; z*=fl; return *this; }
	inline Vector& Div( float fl ) { x/=fl; y/=fl; z/=fl; return *this; }
	inline Vector& MultAdd( const Vector& vec, float fl ) { x+=vec.x*fl; y+=vec.y*fl; z+=vec.z*fl; return *this; }

	inline Vector& Abs() { x = fabs(x); y = fabs(y); z = fabs(z); return *this; }
	inline Vector& Neg() { x = -x; y = -y; z = -z; return *this; }
	inline Vector& Zero() { x = 0.0f, y = 0.0f; z = 0.0f; return *this; }
	
	inline float Length() const { return sqrt( x*x + y*y + z*z ); }
	inline float LengthSqr() const { return x*x + y*y + z*z; }
	inline float DistTo( const Vector& vec ) const { return sqrt( (x-vec.x)*(x-vec.x) + (y-vec.y)*(y-vec.y) + (z-vec.z)*(z-vec.z) ); }
	inline float DistToSqr( const Vector& vec ) const { return (x-vec.x)*(x-vec.x) + (y-vec.y)*(y-vec.y) + (z-vec.z)*(z-vec.z); }

	inline float Norm() { float len = Length()+1.0e-7f; Mult( 1.0f/len ); return len; }
	inline float Dot( const Vector& vec ) const { return x*vec.x + y*vec.y + z*vec.z; }
	inline Vector& Cross( const Vector& vec ) {  }
	inline Vector& Cross( const Vector& vec, Vector& out ) { out.x = y*vec.z - z*vec.y; out.y = z*vec.x - x*vec.z; out.z = x*vec.y - y*vec.x; return out; }
	inline void Resize( float length ) { Mult( length / (Length()+1.0e-7f) ); }

	inline Vector& operator+= ( const Vector& vec ) { return Add( vec ); }
	inline Vector& operator-= ( const Vector& vec ) { return Sub( vec ); }
	inline Vector& operator*= ( float fl ) { return Mult( fl ); }
	inline Vector& operator/= ( float fl ) { return Div( fl ); }

	inline bool IsInBox( const Vector& mins, const Vector& maxs ) const
	{
		return ( x>mins.x && x<maxs.x ) && ( y>mins.y && y<maxs.y ) && ( z>mins.z && z<maxs.z );
	}

	void ToAngle( QAngle& out ) const;

public:
	float x, y, z;
};
class __declspec(align(16)) VectorAligned : public Vector
{
public:
	VectorAligned() { }
	VectorAligned( float xyz ) : Vector(xyz) { }
	VectorAligned( float x, float y, float z ) : Vector(x,y,z) { }
	VectorAligned( const VectorAligned& vec ) : Vector(vec) { }
	VectorAligned( const Vector& vec ) : Vector(vec) { }
	VectorAligned( float* data ) : Vector(data) { }
public:
	float w;
};
class QAngle
{
public:
	QAngle() { }
	QAngle( float xyz ) : pitch(xyz), yaw(xyz), roll(xyz) { }
	QAngle( float x, float y, float z ) { this->pitch = x; this->yaw = y; this->roll = z; }
	QAngle( const QAngle& ang ) : pitch(ang.pitch), yaw(ang.yaw), roll(ang.roll) { }
	QAngle( float* data ) : pitch(data[0]), yaw(data[1]), roll(data[2]) { }

	void ToVector( Vector& forward ) const;
	void ToVector( Vector& forward, Vector& side, Vector& up ) const;

	inline QAngle& Zero() { x = 0.0f; y = 0.0f; z = 0.0f; return *this; }
	
	inline QAngle& Add( const QAngle& ang ) { pitch += ang.pitch; yaw += ang.yaw; roll += ang.roll; return *this; }
	inline QAngle& Sub( const QAngle& ang ) { pitch -= ang.pitch; yaw -= ang.yaw; roll -= ang.roll; return *this; }
	
	inline QAngle& operator+= ( const QAngle& ang ) { return Add(ang); }
	inline QAngle& operator-= ( const QAngle& ang ) { return Sub(ang); }

public:
	union { float pitch; float x; };
	union { float yaw; float y; };
	union { float roll; float z; };
};

#endif // !HGUARD_SDK_VECTOR
