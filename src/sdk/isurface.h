#pragma once

#include "../tools/toolkit.h"
using mobius::getvfunc;

// Fix isurface.h!!!
class ISurface
{
public:
	
	// adds to the font
	enum EFontFlags
	{
		FONTFLAG_NONE,
		FONTFLAG_ITALIC			= 0x001,
		FONTFLAG_UNDERLINE		= 0x002,
		FONTFLAG_STRIKEOUT		= 0x004,
		FONTFLAG_SYMBOL			= 0x008,
		FONTFLAG_ANTIALIAS		= 0x010,
		FONTFLAG_GAUSSIANBLUR	= 0x020,
		FONTFLAG_ROTARY			= 0x040,
		FONTFLAG_DROPSHADOW		= 0x080,
		FONTFLAG_ADDITIVE		= 0x100,
		FONTFLAG_OUTLINE		= 0x200,
		FONTFLAG_CUSTOM			= 0x400,		// custom generated font - never fall back to asian compatibility mode
		FONTFLAG_BITMAP			= 0x800,		// compiled bitmap font - no fallbacks
	};

    void DrawSetColor(int r, int g, int b, int a)
    {
        typedef void ( __thiscall* OriginalFn )( ISurface*, int, int, int, int );
        getvfunc<OriginalFn>( this, 11 )( this, r, g, b, a );
    }
    void DrawFilledRect(int x0, int y0, int x1, int y1)
    {
        typedef void ( __thiscall* OriginalFn )( ISurface*, int, int, int, int );
        getvfunc<OriginalFn>( this, 12 )( this, x0, y0, x1, y1 );
    }
    void DrawOutlinedRect(int x0, int y0, int x1, int y1)
    {
        typedef void ( __thiscall* OriginalFn )( ISurface*, int, int, int, int );
        getvfunc<OriginalFn>( this, 14 )( this, x0, y0, x1, y1 );
    }
    void DrawSetTextFont(unsigned long font)
    {
        typedef void ( __thiscall* OriginalFn )( ISurface*, unsigned long );
        getvfunc<OriginalFn>( this, 17 )( this, font );
    }
    void DrawSetTextColor(int r, int g, int b, int a )
    {
        typedef void ( __thiscall* OriginalFn )( ISurface*, int, int, int, int );
        getvfunc<OriginalFn>( this, 19 )( this, r, g, b, a );
    }
    void DrawSetTextPos(int x, int y )
    {
        typedef void ( __thiscall* OriginalFn )( ISurface*, int, int );
        getvfunc<OriginalFn>( this, 20 )( this, x, y );
    }
    void DrawPrintText(const wchar_t *text, int textLen )
    {
        typedef void ( __thiscall* OriginalFn )( ISurface*, const wchar_t *, int, int );
        return getvfunc<OriginalFn>( this, 22 )( this, text, textLen, 0 );
    }
    unsigned long CreateFont( )
    {
        typedef unsigned long ( __thiscall* OriginalFn )( ISurface* );
        return getvfunc<OriginalFn>( this, 65+1 )( this );
    }
    void SetFontGlyphSet (unsigned long &font, const char *windowsFontName, int tall, int weight, int blur, int scanlines, int flags )
    {
        typedef void ( __thiscall* OriginalFn )( ISurface*, unsigned long, const char*, int, int, int, int, int, int, int );
        getvfunc<OriginalFn>( this, 66+1 )( this, font, windowsFontName, tall, weight, blur, scanlines, flags, 0, 0 );
    }
    void GetTextSize(unsigned long font, const wchar_t *text, int &wide, int &tall)
    {
        typedef void ( __thiscall* OriginalFn )( ISurface*, unsigned long, const wchar_t *, int&, int& );
        return getvfunc<OriginalFn>( this, 73 )( this, font, text, wide, tall );
    }
};

