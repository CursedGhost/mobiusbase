
#include "mathlib.h"

extern void VectorAngles( const Vector &forward, QAngle &angles );
extern void VectorAngles( const Vector &forward, const Vector &pseudoup, QAngle &angles );
void Vector::ToAngle( QAngle& out ) const
{
	VectorAngles( *this, out );
	
	//float	tmp, yaw, pitch;
	//
	//if (forward[1] == 0 && forward[0] == 0)
	//{
	//	yaw = 0;
	//	if (forward[2] > 0)
	//		pitch = 270;
	//	else
	//		pitch = 90;
	//}
	//else
	//{
	//	yaw = (atan2(forward[1], forward[0]) * 180 / M_PI);
	//	if (yaw < 0)
	//		yaw += 360;

	//	tmp = FastSqrt (forward[0]*forward[0] + forward[1]*forward[1]);
	//	pitch = (atan2(-forward[2], tmp) * 180 / M_PI);
	//	if (pitch < 0)
	//		pitch += 360;
	//}
	//
	//angles[0] = pitch;
	//angles[1] = yaw;
	//angles[2] = 0;

}
void QAngle::ToVector( Vector& forward ) const
{
	float	sp, sy, cp, cy;
	
	float ry = DEG2RAD(yaw);
	sy = sinf( ry );
	cy = cosf( ry );
	float rp = DEG2RAD(pitch);
	sp = sinf(rp);
	cp = cosf(rp);
	
	forward.x = cp*cy;
	forward.y = cp*sy;
	forward.z = -sp;
}
extern void AngleVectors( const QAngle& ang, Vector*, Vector*, Vector* );
void QAngle::ToVector( Vector& forward, Vector& right, Vector& up ) const
{
	//float sr, sp, sy, cr, cp, cy;

	//float ry = DEG2RAD(yaw);
	//sy = sinf(ry);
	//cy = sinf(ry);
	//float rp = DEG2RAD(pitch);
	//sp = sinf(rp);
	//cp = sinf(rp);
	//float rr = DEG2RAD(roll);
	//sr = sinf(rr);
	//cr = sinf(rr);

	//forward.x = cp*cy;
	//forward.y = cp*sy;
	//forward.z = -sp;

	//right.x = (-1*sr*sp*cy+-1*cr*-sy);
	//right.y = (-1*sr*sp*sy+-1*cr*cy);
	//right.z = -1*sr*cp;

	//up.x = (cr*sp*cy+-sr*-sy);
	//up.y = (cr*sp*sy+-sr*cy);
	//up.z = cr*cp;

	AngleVectors( *this, &forward, &right, &up ); 
}
