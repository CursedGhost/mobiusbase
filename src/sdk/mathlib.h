#ifndef HGUARD_SDK_MATHLIB
#define HGUARD_SDK_MATHLIB

#include "vector.h"

#ifndef RAD2DEG
	#define RAD2DEG( x  )  ( (float)(x) * (float)(180.f / 3.1415926536f) )
#endif

#ifndef DEG2RAD
	#define DEG2RAD( x  )  ( (float)(x) * (float)(3.1415926536f / 180.f) )
#endif



class Vector;
class QAngle;


//void VectorAngles( const Vector &forward, QAngle &angles );
//void AngleVectors (const QAngle& angles, Vector *forward);
//void AngleVectors (const QAngle& angles, Vector *forward, Vector *right, Vector *up);


struct matrix3x4_t
{
	matrix3x4_t() {}
	matrix3x4_t( 
		float m00, float m01, float m02, float m03,
		float m10, float m11, float m12, float m13,
		float m20, float m21, float m22, float m23 )
	{
		m_flMatVal[0][0] = m00;	m_flMatVal[0][1] = m01; m_flMatVal[0][2] = m02; m_flMatVal[0][3] = m03;
		m_flMatVal[1][0] = m10;	m_flMatVal[1][1] = m11; m_flMatVal[1][2] = m12; m_flMatVal[1][3] = m13;
		m_flMatVal[2][0] = m20;	m_flMatVal[2][1] = m21; m_flMatVal[2][2] = m22; m_flMatVal[2][3] = m23;
	}

	//-----------------------------------------------------------------------------
	// Creates a matrix where the X axis = forward
	// the Y axis = left, and the Z axis = up
	//-----------------------------------------------------------------------------
	//void Init( const Vector& xAxis, const Vector& yAxis, const Vector& zAxis, const Vector &vecOrigin )
	//{
	//	m_flMatVal[0][0] = xAxis.x; m_flMatVal[0][1] = yAxis.x; m_flMatVal[0][2] = zAxis.x; m_flMatVal[0][3] = vecOrigin.x;
	//	m_flMatVal[1][0] = xAxis.y; m_flMatVal[1][1] = yAxis.y; m_flMatVal[1][2] = zAxis.y; m_flMatVal[1][3] = vecOrigin.y;
	//	m_flMatVal[2][0] = xAxis.z; m_flMatVal[2][1] = yAxis.z; m_flMatVal[2][2] = zAxis.z; m_flMatVal[2][3] = vecOrigin.z;
	//}

	//-----------------------------------------------------------------------------
	// Creates a matrix where the X axis = forward
	// the Y axis = left, and the Z axis = up
	//-----------------------------------------------------------------------------
	//matrix3x4_t( const Vector& xAxis, const Vector& yAxis, const Vector& zAxis, const Vector &vecOrigin )
	//{
	//	Init( xAxis, yAxis, zAxis, vecOrigin );
	//}

	inline void Invalidate( void )
	{
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				m_flMatVal[i][j] = 0.0f;
			}
		}
	}

	float *operator[]( int i )				{ return m_flMatVal[i]; }
	const float *operator[]( int i ) const	{ return m_flMatVal[i]; }
	float *Base()							{ return &m_flMatVal[0][0]; }
	const float *Base() const				{ return &m_flMatVal[0][0]; }

	float m_flMatVal[3][4];
};

class Quaternion
{
public:
	float x, y, z, w;
};
class RadianEuler
{
public:
	float x, y, z;
};
class Quaternion48;
class Vector48;
class Quaternion64;
typedef unsigned short float16;

//void MatrixAngles( const matrix3x4_t & matrix, float *angles ); // !!!!
//void MatrixVectors( const matrix3x4_t &matrix, Vector* pForward, Vector *pRight, Vector *pUp );
//void VectorTransform (const float *in1, const matrix3x4_t & in2, float *out);
//void VectorITransform (const float *in1, const matrix3x4_t & in2, float *out);
//void VectorRotate( const float *in1, const matrix3x4_t & in2, float *out);
//void VectorRotate( const Vector &in1, const QAngle &in2, Vector &out );
//void VectorRotate( const Vector &in1, const Quaternion &in2, Vector &out );
//void VectorIRotate( const float *in1, const matrix3x4_t & in2, float *out);

void VectorTransform (const Vector& in1, const matrix3x4_t & in2, Vector& out);



#endif // !HGUARD_SDK_MATHLIB
