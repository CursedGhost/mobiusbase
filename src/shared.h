#pragma once

#include "mobius.h"
#include "ifaces.h"

namespace mobius
{

enum action_t
{
	// Continue running callbacks
	ACTION_CONTINUE,
	// Stop handling callbacks, run original hooked function
	ACTION_HANDLED,
	// Stop handling callbacks, do not run original hooked function
	ACTION_SKIPHOOK,
};

struct usermsg_t
{
	int type;
	const char* name;
	unsigned int name_hash;
	unsigned long size;
	unsigned char* data;
};

struct emitsound_t
{
	int iEntIndex;
	int iChannel;
	union {
		const char* pSample;
		int iSentenceIndex;
	};
	float flVolume;
	union {
		float flAttenuation;
		int iSoundLevel;
	};
	int iFlags;
	int iPitch;
	const Vector* pDirection;
	const Vector* pOrigin;
};

struct createmove_t
{
	CUserCmd* pCommand;
	int sequence_number;
	float frametime;
};


// Base for the 'Vars' context hierarchy.
//
// Provide context state for callbacks.

class CBaseVars
{
	friend class CCheatManager;

public:
	CBaseVars(CCheatManager* base);

public:
	// Link back to the cheat manager
	CCheatManager* cheats;
	// Game independent time
	float flTime;
	// First callback after connecting to a server
	bool bConnected;

protected:
	action_t action;
};


// Basic state 
class CSharedVars : public CBaseVars
{
public:
	typedef CBaseVars BaseClass;

	CSharedVars(CCheatManager* base, QAngle& va);
	static void ClampAngles(QAngle& ang, bool clip = true);

	// WHERE DO I BELONG?
	float AngleDiff(const QAngle& ang, QAngle& delta);

	bool IsValidAngles();

public:
	// Information about you as a player
	IClientEntity* pMe;
	IClientEntity* pWeapon;
	QAngle& va;
	Vector vecMe;
	int player;
	int userID;
	int iMyTeam;
};

// Simple camera struct
class Camera
{
public:
	Camera();
	Camera(int width, int height, const Vector& origin, const QAngle& angles);
	void CalcFOV(float fov, bool desired);
public:
	int width;
	int height;
	Vector origin;
	QAngle angles;
	float fov_desired;
	float fov_x;
	float fov_y;
	float aspect;
};

class CPaintVars : public CSharedVars
{
public:
	typedef CSharedVars BaseClass;

	CPaintVars(CCheatManager* cheats);

	void Setup(const Camera& cam);

	bool ScreenTransform(const Vector& vec, float& x, float& y);
	bool WorldToScreen(const Vector& vec, int& x, int& y);

public:
	Camera cam;
	VMatrix wts;
};

// Shared vars in Tick callbacks
class CTickVars : public CSharedVars
{
public:
	typedef CSharedVars BaseClass;

	CTickVars(CCheatManager* cheats, createmove_t& cm);

	void Fix(const QAngle& va);

	// Convenience to update the underlying CUserCmd
	void Attack();
	void Attack2();
	void Jump();
	void InSet(int btn);
	void InClear(int btn);
	bool InTest(int btn);

public:
	CUserCmd* ucmd;

	// Don't clamp angles if the game sends out-of-range angles
	// This fixes camera bug during certain TF2 taunts
	bool angles_checked;

	// Only valid after prediction
	struct pred_t
	{
		int nTickBase;
		float curtime;
		QAngle vecPunch;
		float flSpread;
		float flInaccuracy;
	} pred;
};
inline void CTickVars::Attack() { InSet(IN_ATTACK); }
inline void CTickVars::Attack2() { InSet(IN_ATTACK2); }
inline void CTickVars::Jump() { InSet(IN_JUMP); }
inline void CTickVars::InSet(int btn) { ucmd->buttons |= btn; }
inline void CTickVars::InClear(int btn) { ucmd->buttons &= ~btn; }
inline bool CTickVars::InTest(int btn) { return ((ucmd->buttons) & btn) != 0; }

class CViewVars : public CSharedVars
{
public:
	typedef CSharedVars BaseClass;
	CViewVars(CCheatManager* cheats, Camera& cam);

	// Set the FOV to be used by the game
	void SetFOV(float fov);
public:
	Camera& cam;
};
inline void CViewVars::SetFOV(float fov) { cam.CalcFOV(fov, false); }

class CUpdateVars : public CSharedVars
{
public:
	typedef CSharedVars BaseClass;

	CUpdateVars(CCheatManager* cheats);

public:
	QAngle va;
};

class CActionVars : public CUpdateVars
{
public:
	typedef CUpdateVars BaseClass;

	CActionVars(CCheatManager* cheats);

	// Stop callbacks, cancel original hook
	void StopHook();
	// Stop callbacks
	void StopPropagation();
};
inline void CActionVars::StopHook() { action = ACTION_SKIPHOOK; }
inline void CActionVars::StopPropagation() { action = ACTION_HANDLED; }

class CEventVars : public CActionVars
{
public:
	typedef CActionVars BaseClass;
	CEventVars(CCheatManager* cheats, IGameEvent* evt);
public:
	IGameEvent* evt;
};

class CSoundVars : public CActionVars
{
public:
	typedef CActionVars BaseClass;
	CSoundVars(CCheatManager* cheats, emitsound_t& snd);
public:
	emitsound_t& snd;
};

class CMessageVars : public CActionVars
{
public:
	typedef CActionVars BaseClass;
	CMessageVars(CCheatManager* cheats, usermsg_t& msg);
public:
	usermsg_t& msg;
};

class CConnectVars : public CBaseVars
{
public:
	typedef CBaseVars BaseClass;
	CConnectVars(CCheatManager* cheats, SignonState state);
public:
	SignonState state;
};

}
