#pragma once
#include <Windows.h>

//----------------------------------------------------------------
// Base Core Interface
//----------------------------------------------------------------
// Low level initialization and level 0 resource management

#define MOBIUSAPI extern "C" __declspec(dllexport)
#define MOBIUSCALL __stdcall

namespace mobius
{

// Declarations
class IGameIfaces;
class CCheatManager;
class CHookManager;

// Log levels
enum spew_t {
	LINFO,
	LDIAG,
	LDEBUG,
	LWARN,
	LERR,
};

// Main core interface for hack management
class CMobiusBase
{
public:
	CMobiusBase();

	int LoadHack();
	virtual int InitHack() = 0;
	virtual void ShutDown();

	// Quick 'n dirty wrapper to treat an array as a value...
	struct path_t {
		typedef char buf_t[MAX_PATH];
		buf_t buf;
		inline buf_t& c_str() { return buf; }
	};

	// Get file path to custom working directory
	path_t GetFilePath(const char* file);

	// Logging
	void SpewEx(spew_t spew, const char* fmt, va_list va = nullptr);
	void Spew(spew_t spew, const char* fmt, ...);

protected:
	FILE* OpenLog();
	void LogToConsole(spew_t spew, const char* buf);
	void LogToFile(spew_t spew, char* buf, FILE* log);

public:
	// Handles
	HMODULE hmCheat;
	HWND hGameWindow;
	FILE* hLogFile;

	// Cheat resources
	IGameIfaces* game;
	CCheatManager* cheats;
	CHookManager* hooks;

	// Our custom working directory
	path_t wdir;
};

// Yeah this isn't the correct way to do error handling but fuck it.
// For those interested: you're supposed to create exception classes for each type of exceptional behaviour
//   instead of having one �ber exception class used for everything.
void ThrowError(const char* fmt, ...);

}

// Wrappers to kick off the process
MOBIUSAPI int MOBIUSCALL LoadHack();
MOBIUSAPI void MOBIUSCALL SignalExit();
