#pragma once

//----------------------------------------------------------------
// Cheat Manager
//----------------------------------------------------------------
// Responsible for managing the cheat modules and providing their callbacks.
// The Hook Manager should be the only one to call these callbacks!

#include "mobius.h"
#include "cheat.h"

namespace mobius
{
class CBaseVars;

class CCheatManager
{
	typedef std::vector<callback_t*> filtered_t;

public:
	CCheatManager();
	virtual ~CCheatManager();

	// Provides Init calls to all registered cheats
	virtual void Init(CMobiusBase* base);
	// Provides Close calls to all fully initialized cheats
	virtual void Shutdown();

	//--------------------------------
	// Management
	//--------------------------------

	// Handle crashes
	void Crashed(callback_t& cb, const std::exception& err);
	// Register a cheat
	// NOTE! Cheats are delete'd in the destructor, make sure you alloc them with new.
	void Register(ICheat* ch);
	// Adds a callback for a registered cheat
	void AddCallback(callback_t& cb);
	// Filters the callback list
	void Filter(filtered_t& out, cbt_t type);
	// Lookup a cheat resource by name
	ICheat* Lookup(const char* id);

	//--------------------------------
	// Callbacks
	//--------------------------------
	// Hooks should call in these, the manager will give all the cheats their callbacks

	void CreateMove(createmove_t& cm);
	bool Update();
	void Paint(CViewSetup& view);
	void ViewSetup(CViewSetup& view);
	bool Connect(SignonState state);
	action_t GameEvent(IGameEvent* evt);
	action_t UserMessage(usermsg_t& msg);
	action_t EmitSound(emitsound_t& snd);

protected:
	// Override this and do client prediction here, fill in vars.pred vars.
	virtual void PredictMove(CTickVars& vars);

	// Invokes all callbacks for a type.
	void Invoke(cbt_t type, CBaseVars& vars);

protected:
	// Stored cheats
	ICheat* cheats;

	// Stored callbacks
	callback_t* callbacks;

	// Current connection state
	SignonState signon_state;
};

}
