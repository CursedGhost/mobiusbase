#pragma once

//----------------------------------------------------------------
// Base Game Interface
//----------------------------------------------------------------
// Game engine / client manipulation

#include "mobius.h"
#include "sdk.h"

class IVEngineClient;
class IGameEventManager2;
class IEngineTrace;
class IEngineSound;
class IEngineVGui;
class IVModelInfoClient;
class IVModelRender;
class IVDebugOverlay;
class IVRenderView;
class CGlobalVarsBase;
class CClientState;
typedef struct player_info_s player_info_t;
class INetChannelInfo;
class INetChannel;

class IStudioRender;
class IMaterialSystem;
		
class ICvar;
struct alias_t;

class IBaseClientDLL;
class IClientEntityList;
class IClientTools;
class IAchievementMgr;
class IGameResources;
class IGameMovement;
class IPrediction;
class IInput;
class CUserMessages;
class CHud;
class IClientMode;
class IClientEntity;

namespace vgui
{
class ISurface;
class IPanel;
}
class IPhysicsSurfaceProps;
class IPhysics;
class IInputSystem;

class IShaderDeviceMgr;

typedef IGameResources* (__cdecl* GameResourcesFn)();

typedef struct HINSTANCE__ *HMODULE;
struct IDirect3D9;
struct IDirect3DDevice9;

class ISteamClient;
class ISteamUser;
class ISteamFriends;
class ISteamUtils;
class ISteamUserStats;
class ISteamApps;

namespace mobius
{

// I really don't like this but I don't see an elegant way out...
extern class IGameIfaces* gpGame;

class IGameIfaces
{
public:
	IGameIfaces();
	virtual ~IGameIfaces() { }
	virtual void Init() = 0;

	// ---- Engine Funcs ----

	virtual void NET_SetConVar(ConVar* cvar, const char* value) = 0;
	virtual bool GetPlayerInfo(int player, player_info_t& pinfo) = 0;
	virtual int GetLocalPlayer() = 0;
	virtual int GetPlayerForUserID(int userid) = 0;
	virtual void GetViewAngles(QAngle& va) = 0;
	virtual void SetViewAngles(const QAngle& va) = 0;
	virtual SignonState GetSignonState() = 0;
	virtual const char* GetLevelName() = 0;
	virtual INetChannel* GetNetChannel() = 0;
	virtual void ConNPrintf(int pos, char* str) = 0;
	virtual void ClientCmd(const char* cmd) = 0;
	virtual void ServerCmd(const char* cmd) = 0;
			int FindPlayerInServer(const char* partial, player_info_t& pinfo);

	// --- Client funcs ---

	virtual ClientClass* GetAllClasses() = 0;
	virtual int PseudoRandom(int seed) = 0;
			float GetInterpolation();

	virtual int SharedRandomInt(int randomSeed, const char* sharedName, int minVal, int maxVal, int baseSeed = 0) = 0;
	virtual float SharedRandomFloat(int randomSeed, const char* sharedName, float minVal, float maxVal, int baseSeed = 0) = 0;

	// --- Cvar funcs ---

	virtual alias_t* FindAlias( const char* name ) = 0;
	virtual ConCommandBase* FindCvar( const char* name ) = 0;
			ConCommandBase* GetCvars();
			ConVar* FindConVar( const char* name, bool check = true );
			ConCommand* FindCommand( const char* name, bool check = true );

	// Module handles

	HMODULE hmEngine;
	HMODULE hmClient;
	HMODULE hmServer;
	HMODULE hmMaterial;
	HMODULE hmTier0;
	HMODULE hmVGuiMs;
	HMODULE hmInputSystem;
	HMODULE hmValve;

	// CreateInterfaceFns

	CreateInterfaceFn pfnAppSystem;
	CreateInterfaceFn pfnClient;

	// Game Resources

	GameResourcesFn pfnGameResources;

	// Interfaces

	IVEngineClient*			pEngine;
	IGameEventManager2*		pEvents;
	IEngineTrace*			pTrace;
	IEngineSound*			pSound;
	IEngineVGui*			pVGUI;
	IVModelInfoClient*		pModelInfo;
	IVModelRender*			pModelRender;
	IVDebugOverlay*			pOverlay;
	IVRenderView*			pRenderView;
	CGlobalVarsBase*		pGlobals;
	CClientState*			pClState;

	IStudioRender*			pStudioRender;
	IMaterialSystem*		pMaterial;

	ICvar*					pCvar;

	IBaseClientDLL*			pClient;
	IClientEntityList*		pEntityList;
	IClientTools*			pTools;
	IAchievementMgr*		pAchMgr;
	IInput*					pInput;
	IGameMovement*			pGameMovement;
	IPrediction*			pPrediction;
	CUserMessages*			pUserMessages;
	IClientMode**			ppClientMode;
	IClientEntity**			ppGameRes;

	typedef int (__cdecl* SharedRandomIntFn)( const char*, int, int, int );
	typedef float (__cdecl* SharedRandomFloatFn)( const char*, float, float, int );
	SharedRandomIntFn		pfnSharedRandomInt;
	SharedRandomFloatFn		pfnSharedRandomFloat;
	typedef unsigned int (__cdecl* MD5_PseudoRandomFn)(unsigned int);
	MD5_PseudoRandomFn		pfnMD5_PseudoRandom;

	vgui::ISurface*			pSurface;
	vgui::IPanel*			pPanels;
	IPhysicsSurfaceProps*	pPhysicsSurfaceProps;
	IInputSystem*			pInputSystem;

	struct steam_t
	{
		bool Init();

		HMODULE hModule;

		ISteamClient*			pClient;
		ISteamUser*				pUser;
		ISteamFriends*			pFriends;
		ISteamUtils*			pUtils;
		ISteamUserStats*		pUserStats;
		ISteamApps*				pApps;
	} Steam;

	steam_t* GetSteam();
};


inline ConCommandBase* IGameIfaces::GetCvars()
{
	return FindCvar( nullptr );
}
// Need SDK for these!
inline ConVar* IGameIfaces::FindConVar( const char* name, bool check )
{
	ConCommandBase* p = FindCvar( name );
	return check ? ( ( p && !p->IsCommand() )? static_cast<ConVar*>( p ): nullptr ) : (ConVar*)p;
}
inline ConCommand* IGameIfaces::FindCommand( const char* name, bool check )
{
	ConCommandBase* p = FindCvar( name );
	return check ? ( ( p && p->IsCommand() )? static_cast<ConCommand*>( p ): nullptr ) : (ConCommand*)p;
}
inline IGameIfaces::steam_t* IGameIfaces::GetSteam()
{
	return Steam.hModule? &Steam: nullptr;
}

}
