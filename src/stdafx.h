// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once
#include "targetver.h"

// Windows headers
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <intrin.h>

// C includes
#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <ctime>

// C++ includes
#include <exception>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
