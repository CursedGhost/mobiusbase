// mobius.cpp : Defines the exported functions for the DLL application.

#include "stdafx.h"

#include "mobius.h"
#include "hooks.h"
#include "cheatmgr.h"
#include "ifaces.h"
#include "tools/printf.hpp"

extern "C" _IMAGE_DOS_HEADER __ImageBase;

namespace mobius
{

CMobiusBase::CMobiusBase() : hmCheat(NULL), hGameWindow(NULL), hLogFile(NULL), game(nullptr), cheats(nullptr), hooks(nullptr) {}
int CMobiusBase::LoadHack()
{
	// Initialize basics
	hmCheat = (HMODULE)&__ImageBase;
	::srand(static_cast<unsigned int>(__rdtsc()));

	::GetModuleFileNameA(hmCheat, wdir.c_str(), sizeof(wdir));
	wdir.remove_filename();

	try {
		// Attempt to initialize the hack
		int code = InitHack();
		if (code) {
			Spew(LERR, "LoadHack() Failure (%d)!", code);
		}
		return code;
	}
	catch (const std::exception& err) {
		// Some error happened
		Spew(LERR, "LoadHack() Unhandled Exception!\n %s", err.what());
		return -1;
	}
}
int CMobiusBase::InitHack()
{
	Spew( LINFO, "InitHack() Success!" );
	return 0;
}
void CMobiusBase::ShutDown()
{
	// Unhook so no new hooks are called
	if (hooks) {
		hooks->Unhook();
	}

	// Wait for any hooks to finish running
	// All hooks should be unhooked at this point!
	CHookManager::WaitForHooks();

	// Clean up resources
	delete hooks;
	hooks = nullptr;
	delete cheats;
	cheats = nullptr;
	delete game;
	game = nullptr;

	// Print a success message
	SpewEx(LINFO, "Shutdown() Done!");

	// And finally close the log file
	if (hLogFile && hLogFile != INVALID_HANDLE_VALUE) {
		::fclose(hLogFile);
		hLogFile = NULL;
	}
}
void CMobiusBase::Spew(spew_t spew, const char* fmt, ...)
{
	va_list va;
	va_start(va, fmt);
	SpewEx(spew, fmt, va);
}
void CMobiusBase::SpewEx(spew_t spew, const char* fmt, va_list va)
{
	char buf[4000];
	vsnprintf(buf, sizeof(buf), fmt, va);

	LogToConsole(spew, buf);

	if (FILE* h = OpenLog()) {
		LogToFile(spew, buf, h);
	}
}
auto CMobiusBase::GetFilePath(const char* file) -> path_t
{
	path_t path = wdir;
	strcat_s(path.c_str(), file);
	return path;
}
FILE* CMobiusBase::OpenLog()
{
	// First run: hLogFile will be NULL
	// On failure: It'll always be -1
	// On success: It'll always be a valid handle
	// Returns NULL if NULL or -1, a valid handle otherwise
	FILE* log = hLogFile;
	if (!log) {
		auto path = GetFilePath("log.txt");

		// Try to open the log file
		log = ::fopen(path.c_str(), "a");

		// Failed to open the log file, bail out
		if (!log) {
			LogToConsole(LERR, "OpenLog() Failed to open log file!!");
			log = (FILE*)INVALID_HANDLE_VALUE;
		}
		hLogFile = log;
	}
	return (log == INVALID_HANDLE_VALUE) ? nullptr : log;
}
void CMobiusBase::LogToConsole(spew_t spew, const char* buf)
{
	static const unsigned int colors[] = {
		0xFFD8D8D8,
		0xFF80C880,
		0xFF80C880,
		0xFF46DCDC,
		0xFF3F3FD2,
		0xFFC0C0C0,
	};
	assert(spew >= 0 && spew < (sizeof(colors) / sizeof(colors[0])));
	ConColorMsg(0, colors[spew], "%s\n", buf);
}
void CMobiusBase::LogToFile(spew_t spew, char* buf, FILE* log)
{
	// Get the system time and format it
	char timestr[64];
	time_t rawtime;
	tm timeinfo;
	::time(&rawtime);
	::localtime_s(&timeinfo, &rawtime);
	::asctime_s(timestr, sizeof(timestr), &timeinfo);
	timestr[strlen(timestr) - 1] = 0; // Get rid of the added newline

	// Prefixes
	static const char prefixes[][8] = { "[INFO] ", "[DIAG] ", "[DBG]  ", "[WARN] ", "[ERR]  " };

	// Render to file line by line
	// Note! this will destroy the buffer!
	char* s = buf;
	bool c;
	do {
		// Find end of line and write a zero terminator
		char* e = s;
		while (*e && *e != '\n') ++e;
		c = *e != 0;
		*e = 0;

		// Ignore empty lines
		if (*s) {
			::fprintf(log, "%s(%s) %s\n", static_cast<const char*>(prefixes[spew]), timestr, s);
		}

		s = e + 1;
	} while (c);

	// Ensure the log is flushed for errors and warnings
	if (spew == LWARN || spew == LERR) {
		::fflush(log);
	}
}



void ThrowError(const char* fmt, ...)
{
	// Format error message
	char buf[4000];
	va_list va;
	va_start(va, fmt);
	vsnprintf(buf, sizeof(buf), fmt, va);
	va_end(va);

	throw std::exception(buf);
}

}
