#include "stdafx.h"

#include "ifaces.h"

namespace mobius
{

IGameIfaces* gpGame = nullptr;

IGameIfaces::IGameIfaces()
{
	hmEngine = ::GetModuleHandleA("engine.dll");
	hmClient = ::GetModuleHandleA("client.dll");
	hmServer = ::GetModuleHandleA("server.dll");
	hmMaterial = ::GetModuleHandleA("MaterialSystem.dll");
	hmTier0 = ::GetModuleHandleA("tier0.dll");
	hmVGuiMs = ::GetModuleHandleA("vguimatsurface.dll");
	hmInputSystem = ::GetModuleHandleA("inputsystem.dll");
	hmValve = ::GetModuleHandleA("vstdlib.dll");

	if (!(hmEngine && hmClient && hmServer && hmMaterial && hmTier0 && hmVGuiMs && hmInputSystem && hmValve)) {
		ThrowError("IGameIfaces::IGameIfaces()", "Failed to acquire HMODULEs, wrong process?");
	}

	gpGame = this;
}
bool IGameIfaces::steam_t::Init()
{
	if ((hModule = ::GetModuleHandleA("steam_api.dll"))) {
		typedef bool (__cdecl* InitSafeFn)();
		typedef ISteamClient* (__cdecl* SteamClientFn)();
		typedef HSteamUser (__cdecl* GetSteamUserFn)();
		typedef HSteamPipe (__cdecl* GetSteamPipeFn)();

		InitSafeFn pfnInitSafe = (InitSafeFn)::GetProcAddress(hModule, "SteamAPI_InitSafe");
		SteamClientFn pfnSteamClient = (SteamClientFn)::GetProcAddress(hModule, "SteamClient");
		GetSteamUserFn pfnGetSteamUser = (GetSteamUserFn)::GetProcAddress(hModule, "GetHSteamUser");
		GetSteamPipeFn pfnGetSteamPipe = (GetSteamPipeFn)::GetProcAddress(hModule, "GetHSteamPipe");

		if (pfnInitSafe && pfnSteamClient && pfnGetSteamUser && pfnGetSteamPipe && pfnInitSafe() && (pClient = pfnSteamClient())) {
			HSteamUser hSteamUser = pfnGetSteamUser();
			HSteamPipe hSteamPipe = pfnGetSteamPipe();

			pUser = pClient->GetISteamUser(hSteamUser, hSteamPipe, "SteamUser008");
			pFriends = pClient->GetISteamFriends(hSteamUser, hSteamPipe, "SteamFriends003");
			pUtils = pClient->GetISteamUtils(hSteamUser, "SteamUtils002");
			pUserStats = pClient->GetISteamUserStats(hSteamUser, hSteamPipe, "STEAMUSERSTATS_INTERFACE_VERSION007");
			pApps = pClient->GetISteamApps(hSteamUser, hSteamPipe, "STEAMAPPS_INTERFACE_VERSION003");
			return true;
		}
		else {
			hModule = NULL;
		}
	}
	return false;
}


int IGameIfaces::FindPlayerInServer(const char* partial, player_info_t& pinfo)
{
	int i = 0, uid, a, b;

	// Specify a userid directly #<userid>
	if (partial[0] == '#' && (uid = atoi(partial + 1))) {
		i = GetPlayerForUserID(uid);
	}
	// Target ourselves
	else if (!strcmp(partial, "!self")) {
		i = GetLocalPlayer();
	}
	// Spectator target
	//else if ( !strcmp( partial, "!spec" ) )
	//{
	//}
	// Player we're aiming at
	//else if (!strcmp( partial, "!aim" ) )
	//{
	//}
	// By SteamID
	else if (sscanf(partial, "STEAM_0:%d:%d", &a, &b)==2) {
		i = b + b + a;
	}
	// Match a name on the server
	else {
		int n = pGlobals->maxClients;

		// Try to find exact match
		for (i = 1; i <= n; ++i) {
			if (GetPlayerInfo(i, pinfo) && !strcmp(pinfo.name, partial)) {
				return i;
			}
		}
		// Try to find a weaker (but still unique) match
		for (i = 1; i <= n; ++i) {
			if (GetPlayerInfo(i, pinfo) && strstr(pinfo.name, partial)) {
				// Find another match
				for (int j = i + 1; j <= n; ++j) {
					if (GetPlayerInfo(j, pinfo) && strstr(pinfo.name, partial)) {
						// Multiple matches detected
						return -2;
					}
				}
				// Unique partial match
				break;
			}
		}
	}
	return GetPlayerInfo(i, pinfo) ? i : -1;
}


float IGameIfaces::GetInterpolation()
{
	// pGlobals->interpolation_amount is always zero...
	// Otherwise use max( cl_interp, cl_interp_ratio / cl_updaterate );

	static ConVar* cl_interp = nullptr;
	static ConVar* cl_interp_ratio = nullptr;
	static ConVar* cl_updaterate = nullptr;

	if ( !cl_interp ) cl_interp = FindConVar( "cl_interp", false );
	if ( !cl_interp_ratio ) cl_interp_ratio = FindConVar( "cl_interp_ratio", false );
	if ( !cl_updaterate ) cl_updaterate = FindConVar( "cl_updaterate", false );

	return max( cl_interp->fValue, cl_interp_ratio->fValue / cl_updaterate->fValue );
}

}
