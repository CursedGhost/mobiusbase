#include "stdafx.h"

#include "shared.h"

namespace mobius
{

CBaseVars::CBaseVars(CCheatManager* base) : cheats(base), flTime(0.f), bConnected(false), action(ACTION_CONTINUE)
{
}

CSharedVars::CSharedVars(CCheatManager* base, QAngle& va) : BaseClass(base), va(va)
{
	player = gpGame->GetLocalPlayer();
	if ((pMe = gpGame->pEntityList->GetClientEntity(player))) {
		player_info_t pinfo;
		if (gpGame->GetPlayerInfo(player, pinfo)) {
			userID = pinfo.userID;
		}
	}
}
void CSharedVars::ClampAngles(QAngle& ang, bool clip)
{
	while (ang.x > 180.0f) {
		ang.x -= 360.0f;
	}
	while (ang.x < -180.0f) {
		ang.x += 360.0f;
	}
	if (clip) {
		if (ang.x > 89.0f) {
			ang.x = 89.0f;
		}
		if (ang.x < -89.0f) {
			ang.x = -89.0f;
		}
	}
	else {
		if (ang.x > 90.0f) {
			ang.x = 180.0f - ang.x;
			ang.y += 180.0f;
		}
		if (ang.x < -90.0f) {
			ang.x = -180.0f - ang.x;
			ang.y += 180.0f;
		}
	}
	while (ang.y > 180.0f) {
		ang.y -= 360.0f;
	}
	while (ang.y < -180.0f) {
		ang.y += 360.0f;
	}
	ang.z = 0.0f;
}
float CSharedVars::AngleDiff(const QAngle& ang, QAngle& delta)
{
	delta = ang;
	delta -= va;

	while (delta.x < -180.0f) {
		delta.x += 360.0f;
	}
	while (delta.x > 180.0f) {
		delta.x -= 360.0f;
	}
	while (delta.y < -180.0f) {
		delta.y += 360.0f;
	}
	while (delta.y > 180.0f) {
		delta.y -= 360.0f;
	}

	return sqrtf(delta.x*delta.x + delta.y*delta.y);
}
bool CSharedVars::IsValidAngles()
{
	return
		va.x >= -89.f &&
		va.x >= 89.f &&
		va.y >= -180.f &&
		va.y <= 180.f;
}


Camera::Camera() {}
Camera::Camera(int width, int height, const Vector& origin, const QAngle& angles)
	: width(width), height(height), origin(origin), angles(angles), aspect(static_cast<float>(width) / static_cast<float>(height))
{
}
void Camera::CalcFOV(float fov, bool desired)
{
	float ratioRatio = aspect / (4.0f / 3.0f);

	// No idea where I ripped these formulas from, but they work :)
	if (desired) {
		// Calculate real fov as needed
		fov_desired = fov;
		fov_x = 2.0f*RAD2DEG(atanf(tanf(DEG2RAD(fov / 2.0f))*ratioRatio));
	}
	else {
		// Calculate the fov_desired fov
		fov_x = fov;
		fov_desired = 2.0f*RAD2DEG(atanf(tanf(DEG2RAD(fov / 2.0f)) / ratioRatio));
	}

	// Derive vertical fov from desired fov
	fov_y = 2.0f*RAD2DEG(atanf(tanf(DEG2RAD(fov_desired / 2.0f)) / (4.0f / 3.0f)));
}



CTickVars::CTickVars(CCheatManager* base, createmove_t& cm) : BaseClass(base, cm.pCommand->viewangles),
	ucmd(cm.pCommand), angles_checked(IsValidAngles())
{
	memset(&pred, 0, sizeof(pred));
}
void CTickVars::Fix(const QAngle& va)
{
	// Last chance to fix bad viewangles
	if (angles_checked) {
		ClampAngles(ucmd->viewangles);
		// Angles are still invalid (NaN etc), just restore them
		if (!IsValidAngles()) {
			ucmd->viewangles = va;
		}
	}

	// Fix movement vectors (only if they changed)
	if (ucmd->viewangles.y != va.y || ucmd->viewangles.x != va.x) {
		float yaw, speed;
		Vector& move = *(Vector*)&ucmd->forwardmove;

		// Movement
		speed = sqrt(move.x*move.x + move.y*move.y);

		// Corrected yaw we want to move at
		yaw = RAD2DEG(atan2(move.y, move.x));
		yaw = DEG2RAD(ucmd->viewangles.y - va.y + yaw);

		// Adjust move
		move.x = cos(yaw) * speed;
		move.y = sin(yaw) * speed;
	}
}

CViewVars::CViewVars(CCheatManager* base, Camera& cam) : BaseClass(base, cam.angles), cam(cam) {}

CUpdateVars::CUpdateVars(CCheatManager* base) : BaseClass(base, va)
{
	gpGame->GetViewAngles(va);
}

CActionVars::CActionVars(CCheatManager* base) : BaseClass(base) {}
CEventVars::CEventVars(CCheatManager* base, IGameEvent* evt) : BaseClass(base), evt(evt) {}
CSoundVars::CSoundVars(CCheatManager* base, emitsound_t& snd) : BaseClass(base), snd(snd) {}
CMessageVars::CMessageVars(CCheatManager* base, usermsg_t& msg) : BaseClass(base), msg(msg) {}
CConnectVars::CConnectVars(CCheatManager* base, SignonState state) : BaseClass(base), state(state) {}

}
